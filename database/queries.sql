-- a (OK)
SELECT waitress1 as employee, count(waitress1)*10 as hours  FROM (SELECT waitress1 FROM shift UNION ALL SELECT waitress2 FROM shift UNION ALL SELECT chef1 FROM shift UNION ALL SELECT chef2 FROM shift UNION ALL SELECT chef3 FROM shift UNION ALL SELECT maid1 FROM shift UNION ALL SELECT maid2 FROM shift UNION ALL SELECT cashier FROM shift UNION ALL SELECT telephonist FROM shift UNION ALL SELECT orderman1 FROM shift UNION ALL SELECT orderman2 FROM shift UNION ALL SELECT headman FROM shift) as tmp GROUP BY waitress1 ORDER BY waitress1

-- b (OK)
SELECT name FROM ingredients WHERE quantity_bound>available_quantity;

-- c (OK)
SELECT d.name FROM dish d LEFT JOIN contains c ON d.name LIKE c.dish LEFT JOIN ingredients i ON c.ingredients LIKE i.name WHERE c.quantity>=i.available_quantity;

-- d (OK)
SELECT t.number, t.description, w.name, w.surname, t.capacity FROM myTable t INNER JOIN employee w ON t.waitress=w.id  WHERE CAST(CAST(t.capacity AS CHAR) AS SIGNED)>6;

-- e (OK)
SELECT * FROM (SELECT ord.idOrder, DATE(ord.date) as Date, te.dishToEat, COUNT(dishToEat) as Counter FROM myOrder ord INNER JOIN toEat te ON ord.idOrder=te.orderToEat GROUP BY DATE(ord.date), te.dishToEat ORDER BY Counter DESC) as d GROUP BY d.Date

-- f 
SELECT

-- g (OK)
SELECT DATE(ord.date) as Date, AVG(ord.price) as avg_cost FROM myORder ord GROUP BY DATE(ord.date);

-- h 
SELECT 

-- i (OK)
SELECT DATE(ord.date) as Date, t.waitress, COUNT(t.waitress) AS Counter FROM myOrder ord INNER JOIN orderFrom ordF ON ord.idOrder=ordF.orderID INNER JOIN myTable t ON t.number=ordF.orderTable GROUP BY DATE(ord.date), t.waitress;

-- j (OK)
SELECT tmp.orderClient AS Client, Date, COUNT(tmp.orderClient) AS Coupons FROM (SELECT ordF.orderClient, DATE(ord.date) AS Date, count(orderClient) as numOfOrders FROM orderFrom ordF INNER JOIN myOrder ord ON ord.idOrder=ordF.orderID GROUP BY YEAR(ord.date), MONTH(ord.date), orderClient) AS tmp WHERE tmp.numOfOrders>=10 GROUP BY tmp.orderClient;

-- 1 poio einai to piato pou zitane tis perissoteres fores allagi sta sistatika
SELECT dish FROM (SELECT dish, COUNT(dish) as Counter FROM extra GROUP BY dish ORDER BY Counter) as tmp;

-- 2 poio einai to poto me tin megaliteri katanalosi
SELECT Drink FROM (SELECT drinkToDrink AS Drink, COUNT(drinkToDrink) AS Counter FROM toDrink GROUP BY drinkToDrink ORDER BY Counter DESC LIMIT 1) AS tmp;

-- 3 poios/poioi ipalilos/opaliloi pernoun ta perissotera xrimata/wra
SELECT id, name, surname FROM employee WHERE salary=(SELECT MAX(salary) FROM employee);

-- 4 poses paragkelies exoun ginei apo to kathe trapezi, taksinomimenes me fthinousa seira etsi wste na fainetai to trapezi me tis perissoteres paragkelies prwto
SELECT orderTable, COUNT(orderTable) AS Counter FROM orderFrom GROUP BY orderTable ORDER BY Counter DESC;

-- 5 gia mia sigkekrimeni paragkelia poses fores zitithike to idio fagito
SELECT dishToEat, COUNT(dishToEat) FROM myOrder ord INNER JOIN toEat te ON te.orderToEat=ord.idOrder WHERE ord.idOrder=(?) GROUP BY dishToEat;