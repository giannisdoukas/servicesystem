package waitress;

import cashier.CashierSceneController;
import com.mysql.jdbc.Connection;
import dbclasses.Client;
import dbclasses.Contains;
import dbclasses.Dish;
import dbclasses.Drinks;
import dbclasses.Ingredients;
import dbclasses.MyOrder;
import dbclasses.MyTable;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.Main;

/**
 * FXML Controller class
 *
 * @author dks
 */
public class WaitressSceneController implements Initializable {
    
    @FXML MenuBar menubar;
    
    @FXML VBox vbox;
            
    GridPane tablesGrid = new GridPane();
    HBox hbox = new HBox(15);
    HBox inputHbox = new HBox(20);    
    Button newClientButton = new Button("New");
    Button submit = new Button("submit");    
    
    private final ComboBox comboTableNum = new ComboBox();
    private final ObservableList<MyTable> comboTableItems = FXCollections.observableArrayList();
    
    private final ComboBox comboClients = new ComboBox();
    private final ObservableList<Client> comboClientsItems = FXCollections.observableArrayList();
    
    private TableView appetizerMenuTable;
    private final ObservableList<Dish> appetizerData = FXCollections.observableArrayList();    
    Button addAppetizerButton = new Button("->"); 
    
    private TableView salatMenuTable;
    private final ObservableList<Dish> salatData = FXCollections.observableArrayList();
    Button addSalatButton = new Button("->");
    
    private TableView soupMenuTable;
    private final ObservableList<Dish> soupData = FXCollections.observableArrayList();
    Button addSoupButton = new Button("->");
    
    private TableView mainMenuTable;
    private final ObservableList<Dish> mainData = FXCollections.observableArrayList();
    Button addMainButton = new Button("->");
    
    private TableView specialMenuTable;
    private final ObservableList<Dish> specialData = FXCollections.observableArrayList();
    Button addSpecialButton = new Button("->");
    
    private TableView drinksMenu;
    private final ObservableList<Drinks> drinks = FXCollections.observableArrayList();
    Button addDrinkButton = new Button("->");
    
    private final GridPane orderPane = new GridPane();
    private TableView toEat;
    private final ObservableList<Dish> toEatData = FXCollections.observableArrayList();
    Button delToEatButton = new Button("<-");    
    private TableView toDrink;
    private final ObservableList<Drinks> toDrinkData = FXCollections.observableArrayList();
    Button delToDrinkButton = new Button("<-");
    Button extraButton = new Button("extra");
    TableView ingredientsTable;
    ObservableList<Ingredients> ingredientsData = FXCollections.observableArrayList();
    TableView ingredientsOfDishTable;
    ObservableList<Contains> ingredientsOfDishData = FXCollections.observableArrayList();
    ButtonType okButtonType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
    ButtonType cancelButtonType = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
    boolean extraEnable = false;
    Dish extraDish = new Dish("");
        
    private final double prefWidth = 300;
    private final double prefHeight = 200;
    
    
    /**
     * Initializes the controller class It creates and visualize the menu table
     * and the table for orders.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        hbox.setAlignment(Pos.CENTER);
        
        submit.setMinWidth(80);
        
        inputHbox.getChildren().add(new Label("Table: "));        
        fillComboTable();
        inputHbox.getChildren().add(comboTableNum);
        inputHbox.getChildren().add(new Label("Client: "));
        fillComboClient();        
        inputHbox.getChildren().add(comboClients);
        inputHbox.getChildren().add(newClientButton);
        tablesGrid.add(inputHbox, 0, 0, 4, 1);
        tablesGrid.setHgap(20);
        tablesGrid.setVgap(10);
        
        delToEatButton.setMinWidth(40);
        
        /**
         * Appetizer Table
         */
        appetizerMenuTable = fillTable("SELECT d.name, d.time, d.instructions, d.price FROM dish d "
                                        + "INNER JOIN appetizer a ON a.appetizer_name LIKE d.name", appetizerData);
        addAppetizerButton.setMinWidth(40);                
        tablesGrid.add(new Label("Appetizer"), 0, 1);
        tablesGrid.add(appetizerMenuTable, 1, 1);
        tablesGrid.add(addAppetizerButton, 2, 1);
        
        /**
         * Salat table
         */
        salatMenuTable = fillTable("SELECT d.name, d.time, d.instructions, d.price FROM dish d "
                                        + "INNER JOIN salat s ON s.salat_name LIKE d.name", salatData);
        addSalatButton.setMinWidth(40);
        tablesGrid.add(new Label("Salat"), 0, 2);
        tablesGrid.add(salatMenuTable, 1, 2);
        tablesGrid.add(addSalatButton, 2, 2);
        
        /**
         * Soup table
         */
        soupMenuTable = fillTable("SELECT d.name, d.time, d.instructions, d.price FROM dish d "
                                        + "INNER JOIN soup s ON s.soup_name LIKE d.name", soupData);
        addSoupButton.setMinWidth(40);
        tablesGrid.add(new Label("Soup"), 0, 3);
        tablesGrid.add(soupMenuTable, 1, 3);
        tablesGrid.add(addSoupButton, 2, 3);
        
        /**
         * Main dish table
         */
        mainMenuTable = fillTable("SELECT d.name, d.time, d.instructions, d.price FROM dish d "
                                        + "INNER JOIN mainDish m ON m.main_name LIKE d.name", mainData);
        addMainButton.setMinWidth(40);
        tablesGrid.add(new Label("Main Dishes"), 0, 4);
        tablesGrid.add(mainMenuTable, 1, 4);
        tablesGrid.add(addMainButton, 2, 4);
        
        /**
         * Special table
         */
        specialMenuTable = fillTable("SELECT d.name, d.time, d.instructions, d.price FROM dish d "
                                        + "INNER JOIN todays_special s ON s.special_name LIKE d.name", specialData);
        addSpecialButton.setMinWidth(40);
        tablesGrid.add(new Label("Special Dishes"), 0, 5);
        tablesGrid.add(specialMenuTable, 1, 5);
        tablesGrid.add(addSpecialButton, 2, 5);
        
        /**
         * Drinks table
         */        
        fillDrinksMenu();
        addDrinkButton.setMinWidth(40);
        tablesGrid.add(new Label("Drinks"), 0, 6);
        tablesGrid.add(drinksMenu, 1, 6);
        tablesGrid.add(addDrinkButton, 2, 6);

        
        hbox.getChildren().add(tablesGrid);
        
        ConstructOrderTable();
        
        orderPane.setHgap(25);
        orderPane.setVgap(10);        
        orderPane.add(toEat, 0, 1, 2, 1);
        orderPane.add(delToEatButton, 2, 1);
        orderPane.add(extraButton, 3, 1);
        orderPane.add(toDrink, 0, 2, 2, 1);        
        orderPane.add(delToDrinkButton, 2, 2);
        orderPane.add(submit, 1, 3);
        
        
        hbox.getChildren().add(orderPane);
        vbox.getChildren().add(hbox);
        
        setupButtons();    
    } // end of init
    
    private void fillComboClient(){
        // get orders and fill the comboBox
        System.out.println("set query");
        String sql = "SELECT c.card_id, c.surname, c.name FROM client c ORDER BY c.surname ASC";
        ResultSet rs;
        try {
            System.out.println("\texecute -> " + sql);
            java.sql.Statement s = Main.getStatement();
            rs = s.executeQuery(sql);
            comboClientsItems.removeAll(comboClientsItems);
            while(rs.next()){
                int card_id = rs.getInt(1);
                String surname = rs.getString(2);
                String name = rs.getString(3);
                Client item = new Client(card_id, surname, name);
                comboClientsItems.add(item);
                System.out.println(item);
            }            
            rs.close();  
            comboClients.setItems(comboClientsItems);
        } 
        catch (SQLException ex) {
            Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void fillComboTable(){
        String sql = "SELECT t.number, t.description, t.capacity FROM myTable t";
        ResultSet rs;
        try {
            System.out.println("\texecute -> " + sql);
            java.sql.Statement s = Main.getStatement();
            rs = s.executeQuery(sql);
            comboTableItems.removeAll(comboTableItems);
            while(rs.next()){
                MyTable item = new MyTable(rs.getInt(1), rs.getInt(3), rs.getString(2));                
                comboTableItems.add(item);
                System.out.println(item);
            }            
            rs.close();  
            comboTableNum.setItems(comboTableItems);
        } 
        catch (SQLException ex) {
            Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private TableView fillTable(String sql, ObservableList data){
        TableView myTable;
        /**
         * create dishes Table
         */
        System.out.println("create table");
        myTable = new TableView();
        myTable.setPrefSize(prefWidth, prefHeight);
        TableColumn nameColumn = new TableColumn("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        TableColumn timeColumn = new TableColumn("Time");
        timeColumn.setCellValueFactory(new PropertyValueFactory("time"));
        TableColumn instructionsColumn = new TableColumn("Instructions");
        instructionsColumn.setCellValueFactory(new PropertyValueFactory("instructions"));
        TableColumn priceColumn = new TableColumn("Price");
        priceColumn.setCellValueFactory(new PropertyValueFactory("price"));        
        myTable.getColumns().addAll(nameColumn, timeColumn, instructionsColumn, priceColumn);
        
        //fill the table from database
        System.out.println("set query");
        ResultSet rs;
        try {
            System.out.println("execute");
            java.sql.Statement s = Main.getStatement();
            rs = s.executeQuery(sql);
            
            while(rs.next()){
                Dish item = new Dish(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getBigDecimal(4));
                data.add(item);
                System.out.println(item);
            }
            rs.close();              
            myTable.setItems(data);
            
            String query = "SELECT ingr.name, con.quantity FROM ingredients ingr "
                                + "INNER JOIN contains con ON con.ingredients LIKE ingr.name "
                                + "WHERE con.dish LIKE (?)";
            System.out.println("execute:\t" + query);
            Connection connection = Main.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement(query);
            for(Object obj : data){
                Dish dish = (Dish) obj;
                prepareStatement.setString(1, dish.getName());   
                ArrayList<Contains> arrayList = new ArrayList();
                rs = prepareStatement.executeQuery();
                while(rs.next()){
                    Contains item = new Contains();                    
                    item.setIngredients1(new Ingredients(rs.getString(1)));
                    item.setQuantity(rs.getInt(2));
                    System.out.println(" \t---> " + item);
                    arrayList.add(item);
                }
                dish.setContainsCollection(arrayList);
            }                       
        } 
        catch (SQLException ex) {
            Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (NullPointerException ex){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning!!!");
            alert.setHeaderText("Probably no connection with Database!");
            
            TextArea textArea = new TextArea(ex.toString());
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(textArea, 0, 1);

            // Set expandable Exception into the dialog pane.
            alert.getDialogPane().setExpandableContent(expContent);

            alert.showAndWait();
            this.goHomeHandler(new ActionEvent());
        }
        return myTable;
    }            
    
    private void fillDrinksMenu(){
        System.out.println("fill drinks menu");
        drinksMenu = new TableView();
        drinksMenu.setPrefSize(prefWidth, prefHeight);
        
        TableColumn nameColumn = new TableColumn("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        TableColumn availableColumn = new TableColumn("Available");
        availableColumn.setCellValueFactory(new PropertyValueFactory("availableQuantity"));
        drinksMenu.getColumns().addAll(nameColumn, availableColumn);
        
        String query = "SELECT d.name, d.available_quantity, d.price FROM drinks d WHERE d.available_quantity>0";
        ResultSet rs;
        try {
            System.out.println("Execute:\t" + query);
            java.sql.Statement s = Main.getStatement();
            rs = s.executeQuery(query);
            
            while(rs.next()){
                Drinks item = new Drinks(rs.getString(1), rs.getInt(2), rs.getBigDecimal(3));
                drinks.add(item);
                System.out.println(" - " + item);
            }
            rs.close();  
            drinksMenu.setItems(drinks);            
        } 
        catch (SQLException ex) {
            Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    private void ConstructOrderTable(){
        /**
         * create toEat table
         */
        toEat = new TableView();
        toEat.setPrefSize(prefWidth, prefHeight);
        
        TableColumn column = new TableColumn("Name");        
        column.setCellValueFactory(new PropertyValueFactory("name"));
        toEat.getColumns().add(column);
        column = new TableColumn("Price");        
        column.setCellValueFactory(new PropertyValueFactory("price"));
        toEat.getColumns().add(column);

        toEat.setItems(toEatData); 
        
        /**
         * create toDrink table
         */
        toDrink = new TableView();
        toDrink.setPrefSize(prefWidth, prefHeight);
        
        column = new TableColumn("Name");        
        column.setCellValueFactory(new PropertyValueFactory("name"));
        toDrink.getColumns().add(column);
        column = new TableColumn("Price");        
        column.setCellValueFactory(new PropertyValueFactory("price"));
        toDrink.getColumns().add(column);
        
        toDrink.setItems(toDrinkData);
    }
    
    private void setupButtons(){
        addAppetizerButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                Dish d = (Dish) appetizerMenuTable.getSelectionModel().getSelectedItem();                
                toEatData.add(d);
                System.out.println(d.getName() + " | " + d.getPrice() + ": add to order");
            }
        });
        
        addSalatButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Dish d = (Dish) salatMenuTable.getSelectionModel().getSelectedItem();                
                toEatData.add(d);
                System.out.println(d.getName() + " | " + d.getPrice() + ": add to order");
            }
        });
        
        addSoupButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Dish d = (Dish) soupMenuTable.getSelectionModel().getSelectedItem();                
                toEatData.add(d);
                System.out.println(d.getName() + " | " + d.getPrice() + ": add to order");
            }
        });
        
        addMainButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Dish d = (Dish) mainMenuTable.getSelectionModel().getSelectedItem();                
                toEatData.add(d);
                System.out.println(d.getName() + " | " + d.getPrice() + ": add to order");
            }
        });
        
        addSpecialButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Dish d = (Dish) specialMenuTable.getSelectionModel().getSelectedItem();                
                toEatData.add(d);
                System.out.println(d.getName() + " | " + d.getPrice() + ": add to order");
            }
        });
        
        addDrinkButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Drinks d = (Drinks) drinksMenu.getSelectionModel().getSelectedItem();                
                toDrinkData.add(d);
                System.out.println(d.getName() + " | " + d.getPrice() + ": add to order");
            }
        });
        
        delToEatButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                Object obj = toEat.getSelectionModel().getSelectedItem();
                toEatData.remove(obj);
                System.out.println(obj + ": removed from order");
            }
        });
        
        delToDrinkButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Object obj = toDrink.getSelectionModel().getSelectedItem();
                toDrinkData.remove(obj);
                System.out.println(obj + ": removed from order");
            }
        });
        
        submit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                try {
                                      
                    MyTable mytable = (MyTable) comboTableNum.getSelectionModel().getSelectedItem();
                    int tmpTableNum = mytable.getNumber();
                    Client client = (Client) comboClients.getSelectionModel().getSelectedItem();
                    if (client != null  &&  mytable != null){                        
                        System.out.println("table: " + tmpTableNum + ", client: " + client.getCardId());                    
                        /**
                         * insert new order
                         */
                       Timestamp timestamp = new Timestamp(new Date().getTime());
                       String strTime = timestamp.toString();
                       strTime = strTime.substring(0, strTime.lastIndexOf("."));
                       System.out.println("Prepare insert new order");
                       Connection conn = Main.getConnection();
                       String query = "INSERT INTO myOrder" +
                               "(date, done, paid) VALUES" +
                               "( (?), false, false )";
                       PreparedStatement preStmt = conn.prepareStatement(query);
                       preStmt.setString(1, strTime);
                       preStmt.executeUpdate();
                       System.out.println("\t" + query.replace("?", strTime) );
                       /**
                        * get orders id
                        */
                       System.out.println("Get orders id");
                       java.sql.Statement s = Main.getStatement();
                       query = "SELECT * FROM myOrder WHERE date = " + "\"" + strTime +"\"";
                       System.out.println("\t" + query);
                       ResultSet rs1 = s.executeQuery(query);
                       int ordID = -1;
                       while (rs1.next()) {
                           ordID = rs1.getInt(1);
                           MyOrder item = new MyOrder(ordID, rs1.getTimestamp(2), rs1.getBoolean(3));
                           System.out.println(item);
                       }
                       rs1.close();
                       System.out.println("Result set closed");

                       /**
                        * prepare insert toEat
                        */
                       System.out.println(" --- ORDER ---");

                       /**
                        * Create dialog
                        */
                       Dialog dialog = new Dialog<>();                    
                       dialog.setTitle("Order " + " at table: " + tmpTableNum);
                       dialog.setHeaderText(strTime);
                       ButtonType loginButtonType = new ButtonType("Send", ButtonBar.ButtonData.OK_DONE);
                       dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

                       GridPane grid = new GridPane();
                       grid.setHgap(10);
                       grid.setVgap(10);
                       grid.setPadding(new Insets(20, 150, 10, 10));
                       grid.add(new Label("DISHES"), 0, 0);
                       grid.add(new Label("-------"), 0, 1);
                       BigDecimal totalPrice = BigDecimal.ZERO;
                       int i=0;
                       for(i=0; i<toEatData.size(); i++){
                           Dish dishPair = toEatData.get(i);
                           String n = dishPair.getName();
                           BigDecimal pr = dishPair.getPrice();
                           totalPrice = totalPrice.add(pr);
                           System.out.println("\t" + n);
                           grid.add(new Label(n), 0, i+2);
                           grid.add(new Label(pr.toString()), 1, i+2);
                       }
                       grid.add(new Label("DRINKS"), 0, i+2);
                       grid.add(new Label("-------"), 0, i+3);
                       for(int j=0; j<toDrinkData.size(); j++){
                           Drinks drink = toDrinkData.get(j);
                           String n = drink.getName();
                           BigDecimal pr = drink.getPrice();
                           totalPrice = totalPrice.add(pr);
                           System.out.println("\t" + n);
                           grid.add(new Label(n), 0, i+2 + j+2);
                           grid.add(new Label(pr.toString()), 1, i+2 + j+2);
                       }
                       grid.add(new Label("Total: "), 0, toEatData.size() + toDrinkData.size() + 4);
                       grid.add(new Label(totalPrice.toString()), 1, toEatData.size() + toDrinkData.size() + 4);

                       dialog.getDialogPane().setContent(grid);
                       Optional result = dialog.showAndWait();

//                       if(result.get() == ButtonBar.ButtonData.OK_DONE){
                       if(result.isPresent()) {
                            // send order
                            // insert dishes
                            System.out.println("send");
                            System.out.println(" - insert toEat");
                            query = "INSERT INTO toEat(orderToEat, dishToEat)" +
                                    " VALUES ( " +
                                    "(SELECT idOrder FROM myOrder WHERE idOrder=(?)), " +
                                    "(SELECT name FROM dish WHERE name LIKE (?)) " +
                                    " )";
                            conn.setAutoCommit(false);
                            preStmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                            int tmp=0;
                            int extra_i=0;
                            for(Dish order : toEatData){
                                System.out.println("\t" + ordID + " -> " + order.getName());
                                if(order.getName().compareTo(extraDish.getName()) == 0){
                                    extra_i = tmp;
                                }
                                preStmt.setInt(1, ordID);
                                preStmt.setString(2, order.getName());
                                preStmt.addBatch();
                                tmp++;
                            }                        
                            int [] upCounts = preStmt.executeBatch();
                            conn.commit();
                            conn.setAutoCommit(true);

                            ResultSet rs = preStmt.getGeneratedKeys();
                            int toEatID = -1;
                            tmp=0;
                            while(rs.next()){                                              
                                int tmpGenerated = rs.getInt(1);
                                System.out.println("Generated key: " + tmpGenerated);
                                if(extra_i == tmp){                                    
                                    toEatID = tmpGenerated;
                                }
                                tmp++;
                            }


                            toEatData.removeAll(toEatData);  

                            //insert extra if enable
                            if(extraEnable == true){
                                query = "INSERT INTO extra(dish, ingredient, toEatID, quantity) VALUES ( "
                                       + "(SELECT dishToEat FROM toEat WHERE toEatID=(?)),  "
                                       + "(SELECT name FROM ingredients WHERE name LIKE (?)), "
                                       + "(SELECT toEatID FROM toEat WHERE toEatID=(?)),  "
                                       + "(?) )";
                                conn.setAutoCommit(false);
                                preStmt = conn.prepareStatement(query);
                                System.out.println(" - (" + toEatID + ")Extra: ");
                                for(Contains contain : ingredientsOfDishData){
                                    preStmt.setInt(1, toEatID);
                                    preStmt.setString(2, contain.getIngredients1().getName());
                                    preStmt.setInt(3, toEatID);
                                    preStmt.setInt(4, contain.getQuantity());
                                    System.out.println("\t" + contain);
                                    preStmt.addBatch();
                                }
                                upCounts = preStmt.executeBatch();
                                conn.commit();
                                conn.setAutoCommit(true);
                            }

                            //inset drinks
                            System.out.println(" - insert toDrink");
                            query = "INSERT INTO toDrink(orderToDrink, drinkToDrink)" +
                                   " VALUES ( " +
                                   "(SELECT idOrder FROM myOrder WHERE idOrder=(?)), " +
                                   "(SELECT name FROM drinks WHERE name LIKE (?)) " +
                                   " )";
                            conn.setAutoCommit(false);
                            preStmt = conn.prepareStatement(query);
                            for(Drinks drink : toDrinkData){
                               System.out.println("\t" + ordID + " -> " + drink.getName());
                               preStmt.setInt(1, ordID);
                               preStmt.setString(2, drink.getName());
                               preStmt.addBatch();
                           }
                           upCounts = preStmt.executeBatch();
                           conn.commit();
                           conn.setAutoCommit(true);
                           toDrinkData.removeAll(toDrinkData);

                           //insert price in order
                           query = "UPDATE myOrder SET price=(?) WHERE idOrder=(?)";
                           preStmt = conn.prepareStatement(query);
                           preStmt.setBigDecimal(1, totalPrice);
                           preStmt.setInt(2, ordID);
                           preStmt.executeUpdate();

                           //insert client who ordered
                           System.out.println(" - insert orderFrom");
                           query = "INSERT INTO orderFrom(orderClient, orderID, orderTable) VALUES ("
                                   + "  (SELECT card_id FROM client WHERE card_id=(?)),"
                                   + "  (SELECT idOrder FROM myOrder WHERE idOrder=(?)),"
                                   + "  (SELECT number FROM myTable WHERE number=(?)) "
                                   + ")";
                           preStmt = conn.prepareStatement(query);
                           preStmt.setInt(1, client.getCardId());
                           preStmt.setInt(2, ordID);
                           preStmt.setInt(3, tmpTableNum);                           
                           preStmt.executeUpdate();
                           System.out.println("\tcardID: " + client.getCardId() 
                                    + "\n\tidOrder: "+ ordID 
                                    + "\n\tTable number: " + tmpTableNum);

                           //update points
                           System.out.println("Update client points");
                           query = "UPDATE client SET points=points+(?) WHERE card_id=(?)";
                           preStmt = conn.prepareStatement(query);
                           int points = (int) (0.1*totalPrice.floatValue());
                           preStmt.setInt(1, points);
                           preStmt.setInt(2, client.getCardId());
                           preStmt.executeUpdate();
                           System.out.println("\t- " + client.getName() + " get " + points);
                           
                       }else{
                           System.out.println("cancel");
                       }
                    } // client != null && table != null
                    else{
                        Alert alert = new Alert(Alert.AlertType.WARNING, "Please choose table and client", ButtonType.OK);
                        alert.showAndWait();
                    }
                }catch (SQLException ex) {
                    Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);
                }catch (NumberFormatException ex){
                    Alert numError = new Alert(Alert.AlertType.ERROR, "On field "
                            + "\"table\" is not a number!", ButtonType.CLOSE);
                    numError.showAndWait();
                }catch (NullPointerException ex){
                    Alert nullError = new Alert(Alert.AlertType.ERROR, "Please fill the fields!", ButtonType.CLOSE);
                    nullError.showAndWait();
                }
            }
        });
    
        newClientButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Dialog inputDialog = new Dialog<>();
                inputDialog.setTitle("New Client");
                inputDialog.setHeaderText("Fill all fields");
                
                ButtonType addButtonType = new ButtonType("Add", ButtonBar.ButtonData.OK_DONE);
                inputDialog.getDialogPane().getButtonTypes().addAll(addButtonType);
                
                GridPane grid = new GridPane();
                grid.setVgap(10);
                grid.setHgap(10);
                grid.setPadding(new Insets(20, 150, 10, 10));
                
                //insert text fields
                TextField nameField = new TextField();
                nameField.setPromptText("name");
                TextField surnameField = new TextField();
                surnameField.setPromptText("surname");
                
                grid.add(new Label("Name: "), 0, 0);
                grid.add(nameField, 0, 1);
                grid.add(new Label("Surname: "), 1, 0);
                grid.add(surnameField, 1, 1);
                
                inputDialog.getDialogPane().setContent(grid);
                
                Optional<ButtonBar.ButtonData> result = inputDialog.showAndWait();
                if(result.isPresent()){
                    try {      
                        if( nameField.getText().length()>0 && surnameField.getText().length()>0 ){
                            String query = " INSERT INTO client(name, surname) VALUES ((?), (?))";
                            System.out.println("\texecute:" + query);
                            Connection conn = Main.getConnection();
                            PreparedStatement prepareStatement = conn.prepareStatement(query);
                            prepareStatement.setString(1, nameField.getText());
                            prepareStatement.setString(2, surnameField.getText());
                            prepareStatement.executeUpdate();
                            System.out.println("insert OK!");
                            fillComboClient();
                        }
                        else{
                            throw new Exception();
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(CashierSceneController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Alert alert = new Alert(Alert.AlertType.WARNING, "Fill name and username", ButtonType.OK);
                        alert.showAndWait();
                    }
                }
                
            }
        });
        
        extraButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Dish dish = (Dish) toEat.getSelectionModel().getSelectedItem();
                if( dish != null){
                    Optional<ButtonType> option = constructExtraDialog(dish).showAndWait();
                    if(option.get() == okButtonType){
                        System.out.println("OK");
                        extraDish.setName(dish.getName());
                        extraEnable = true;
                    }                       
                    else{
                        System.out.println("Cancel");
                        extraEnable = false;
                    }
                }
            }
        });
    }
    
    private Alert constructExtraDialog(Dish dish){
        
        try {
            
            ingredientsTable = new TableView();
            TableColumn nameColumn = new TableColumn("Name");
            nameColumn.setCellValueFactory(new PropertyValueFactory("name"));
            TableColumn availableColumn = new TableColumn("Available quantity");
            availableColumn.setCellValueFactory(new PropertyValueFactory("availableQuantity"));
            ingredientsTable.getColumns().addAll(nameColumn, availableColumn);
            
            ingredientsOfDishTable = new TableView();
            TableColumn ingredientColumn = new TableColumn("Ingredient");
            ingredientColumn.setCellValueFactory(new PropertyValueFactory("ingredients1"));
            TableColumn quantityColumn = new TableColumn("Quantity");
            quantityColumn.setCellValueFactory(new PropertyValueFactory("quantity"));
            ingredientsOfDishTable.getColumns().addAll(ingredientColumn, quantityColumn);
            
            String query = "SELECT i.name, i.available_quantity FROM ingredients i";
            
            //fill the table from database
            System.out.println("set query: " + query);
            ResultSet rs;
                
            System.out.println("execute");
            java.sql.Statement s = Main.getStatement();
            rs = s.executeQuery(query);
            ingredientsData.removeAll(ingredientsData);
            while(rs.next()){
                Ingredients item = new Ingredients(rs.getString(1), rs.getInt(2));
                ingredientsData.add(item);
                System.out.println(item);
            }
            rs.close();
            ingredientsTable.setItems(ingredientsData);            
                        
            query = "SELECT ingredients, quantity FROM contains WHERE dish LIKE (?)";
            PreparedStatement prepareStatement = Main.getConnection().prepareStatement(query);
            prepareStatement.setString(1, dish.getName());             
            rs = prepareStatement.executeQuery();
            System.out.println("ingredients: ");
            ingredientsOfDishData.removeAll(ingredientsOfDishData);
            while(rs.next()){
                Contains item = new Contains(new Ingredients(rs.getString(1)), rs.getInt(2));
                ingredientsOfDishData.add(item);                
                System.out.println("\t- " + item);
            }
            ingredientsOfDishTable.setItems(ingredientsOfDishData);
            
            Alert dialog = new Alert(Alert.AlertType.NONE);
                        
            dialog.getDialogPane().getButtonTypes().addAll(okButtonType, cancelButtonType);
            
            dialog.setTitle("Extra");
            dialog.setHeaderText("Select more/less ingredients");
            GridPane grid = new GridPane();
            grid.setHgap(25);
            
            grid.add(ingredientsTable, 0, 0);
            grid.add(ingredientsOfDishTable, 1, 0);
            
            /**
             * Setup add,remove buttons
             */
            Button addExtraButton = new Button("Add");
            Button removeExtraButton = new Button("Remove");
            
            addExtraButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    Ingredients item = (Ingredients) ingredientsTable.getSelectionModel().getSelectedItem();
                    if(item!=null){                        
                        System.out.println("Extra add: " + item);
                        ObservableList<Contains> tmp = FXCollections.observableArrayList(ingredientsOfDishData);
                        ingredientsOfDishData.removeAll(ingredientsOfDishData);
                        boolean flag = false;
                        for(Contains contain : tmp){
                            System.out.println(" - Before: " + contain.getIngredients1().getName() + ", " + contain.getQuantity());
                            if(item.getName().compareTo(contain.getIngredients1().getName()) == 0){
                                flag = true;
                                contain.setQuantity(contain.getQuantity()+1);
                            }              
                            System.out.println(" - After: " + contain.getIngredients1().getName() + ", " + contain.getQuantity());
                            ingredientsOfDishData.add(contain);                            
                        }
                        if(flag == false){
                            ingredientsOfDishData.add(new Contains(new Ingredients(item.getName()), 1));
                        }
                        ingredientsOfDishTable.setItems(ingredientsOfDishData);                        
                    }
                }
            });
            removeExtraButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    Contains item = (Contains) ingredientsOfDishTable.getSelectionModel().getSelectedItem();
                    if(item!= null){
                        System.out.println("remove: " + item);
                        ObservableList<Contains> tmp = FXCollections.observableArrayList(ingredientsOfDishData);
                        ingredientsOfDishData.removeAll(ingredientsOfDishData);
                        for(Contains contain : tmp){
                            if(item.getIngredients1().getName().compareTo(contain.getIngredients1().getName()) == 0){
                                contain.setQuantity(contain.getQuantity()-1);
                            }
                            if(contain.getQuantity() > 0){
                                ingredientsOfDishData.add(contain);
                            }
                        }
                        ingredientsOfDishTable.setItems(ingredientsOfDishData);
                    }
                }
            });
            
            grid.add(addExtraButton, 0, 1);
            grid.add(removeExtraButton, 1, 1);
            dialog.getDialogPane().setContent(grid);
            return dialog;
        } 
        catch (SQLException ex) {
            Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);            
        }
        return null;
    }
    
    @FXML protected void goHomeHandler(ActionEvent event) {
        Stage stage;
        Parent root;

        stage = (Stage) menubar.getScene().getWindow(); 
        
        try {
            root = FXMLLoader.load(getClass().getResource("/main/mainScene.fxml"));
            Scene scene = new Scene(root, 600, 600);
            stage.setTitle("Welcome");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
}
