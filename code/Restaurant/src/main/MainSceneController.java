package main;

import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Pair;
import static main.Main.conn;
import static main.Main.stmt;

/**
 * FXML Controller class
 *
 * @author dks
 */
public class MainSceneController implements Initializable {
    
    @FXML private Button chefButton;
               
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //
    }    
            
    @FXML protected void goWaitressHandler(ActionEvent event) {
        GoToFXML("/waitress/waitressScene.fxml", "Waitress");
    }
    
    @FXML protected void goCashierHandler(ActionEvent event){
        GoToFXML("/cashier/CashierScene.fxml", "Cashier");
    }
    
    @FXML protected void goChefHandler(ActionEvent event) {
        GoToFXML("/chef/ChefScene.fxml", "Chef");        
    }       
    
    @FXML protected void ConnectionHandler(ActionEvent event){
        //   // JDBC driver name and database URL
        final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
        String DB_URL_prefix = "jdbc:mysql://";
        String DB_URL = "localhost/mydb";
                
        // Create the custom dialog.
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Login");
        dialog.setHeaderText("Connect to Database");

        // Set the icon (must be included in the project).
//        dialog.setGraphic(new ImageView(this.getClass().getResource("login.png").toString()));

        // Set the button types.
        ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField username = new TextField();
        username.setPromptText("Username");
        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        TextField url = new TextField(DB_URL);

        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Password:"), 0, 1);
        grid.add(password, 1, 1);
        grid.add(new Label("Database URL:"), 0, 2);
        grid.add(url, 1, 2);

        // Enable/Disable login button depending on whether a username was entered.
        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);

        // Do some validation (using the Java 8 lambda syntax).
        username.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(() -> username.requestFocus());

        //   //  Database credentials
        String USER = "root";
        String PASS = "";
        
        // Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(username.getText(), password.getText());
            }
            return null;
        });

        Optional<Pair<String, String>> result = dialog.showAndWait();

        if(result.isPresent()){
            USER = username.getText();
            PASS = password.getText();
            DB_URL = url.getText();
            
            try{
                Class.forName("com.mysql.jdbc.Driver");
                System.out.println("Connecting to database...");
                conn = (Connection) DriverManager.getConnection(DB_URL_prefix + DB_URL,USER,PASS);
                stmt = conn.createStatement();
               System.out.println("Connected!");
            }
            catch(ClassNotFoundException | SQLException e){ 
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Exception!");

                TextArea textArea = new TextArea(e.toString());
                textArea.setEditable(false);
                textArea.setWrapText(true);

                textArea.setMaxWidth(Double.MAX_VALUE);
                textArea.setMaxHeight(Double.MAX_VALUE);
                GridPane.setVgrow(textArea, Priority.ALWAYS);
                GridPane.setHgrow(textArea, Priority.ALWAYS);

                GridPane expContent = new GridPane();
                expContent.setMaxWidth(Double.MAX_VALUE);
                expContent.add(textArea, 0, 1);

                // Set expandable Exception into the dialog pane.
                alert.getDialogPane().setExpandableContent(expContent);

                alert.showAndWait();
            }// catch exception
        } // result.present
    }// end of connectionHandler
    
    @FXML protected void CloseHandler(ActionEvent event) {
        try{
            if(stmt!=null){
               stmt.close();
               System.out.println("Statement closed!");
            }
            if(conn!=null){
                conn.close();
                System.out.println("Connection closed!");
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    private void GoToFXML(String path, String title){
        Stage stage;
        Parent root;
        stage = (Stage) chefButton.getScene().getWindow();
        
        try {
            root = FXMLLoader.load(getClass().getResource(path));
            Scene scene = new Scene(root, 600, 600);
            stage.setTitle(title);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
