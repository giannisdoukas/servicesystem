/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import com.mysql.jdbc.Connection;
import java.awt.Rectangle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.sql.*;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

/**
 *
 * @author dks
 */
public class Main extends Application{
    Parent root;
    static Stage stage;
    static Connection conn = null;
    static Statement stmt = null;
    
    @Override
    public void start(Stage s) throws Exception{             
        stage = s;
        root = FXMLLoader.load(getClass().getResource("mainScene.fxml"));
        
        Rectangle2D r = Screen.getPrimary().getBounds();
        Scene scene = new Scene(root, r.getWidth(), r.getHeight());
        stage.setTitle("Welcome");
        stage.setScene(scene);
        stage.show();
    }
    
    static public Connection getConnection(){
        return conn;
    }
    
    static public Statement getStatement(){
        return stmt;
    }
}
