/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "employee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e"),
    @NamedQuery(name = "Employee.findById", query = "SELECT e FROM Employee e WHERE e.id = :id"),
    @NamedQuery(name = "Employee.findByName", query = "SELECT e FROM Employee e WHERE e.name = :name"),
    @NamedQuery(name = "Employee.findBySurname", query = "SELECT e FROM Employee e WHERE e.surname = :surname"),
    @NamedQuery(name = "Employee.findByRecruitmentDate", query = "SELECT e FROM Employee e WHERE e.recruitmentDate = :recruitmentDate"),
    @NamedQuery(name = "Employee.findByDegree", query = "SELECT e FROM Employee e WHERE e.degree = :degree"),
    @NamedQuery(name = "Employee.findByYearsOfService", query = "SELECT e FROM Employee e WHERE e.yearsOfService = :yearsOfService"),
    @NamedQuery(name = "Employee.findBySalary", query = "SELECT e FROM Employee e WHERE e.salary = :salary")})
public class Employee implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "surname")
    private String surname;
    @Column(name = "recruitment_date")
    @Temporal(TemporalType.DATE)
    private Date recruitmentDate;
    @Column(name = "degree")
    private String degree;
    @Column(name = "years_of_service")
    private Integer yearsOfService;
    @Lob
    @Column(name = "cv")
    private String cv;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "salary")
    private BigDecimal salary;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "employee")
    private Cashier cashier;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "employee")
    private Maid maid;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "employee")
    private Headman headman;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "employee")
    private Orderman orderman;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "employee")
    private Waitress waitress;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "employee")
    private Chef chef;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "employee")
    private Telephonist telephonist;

    public Employee() {
    }

    public Employee(Integer id) {
        this.id = id;
    }

    public Employee(Integer id, String name, String surname, BigDecimal salary) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getRecruitmentDate() {
        return recruitmentDate;
    }

    public void setRecruitmentDate(Date recruitmentDate) {
        this.recruitmentDate = recruitmentDate;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Integer getYearsOfService() {
        return yearsOfService;
    }

    public void setYearsOfService(Integer yearsOfService) {
        this.yearsOfService = yearsOfService;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Cashier getCashier() {
        return cashier;
    }

    public void setCashier(Cashier cashier) {
        this.cashier = cashier;
    }

    public Maid getMaid() {
        return maid;
    }

    public void setMaid(Maid maid) {
        this.maid = maid;
    }

    public Headman getHeadman() {
        return headman;
    }

    public void setHeadman(Headman headman) {
        this.headman = headman;
    }

    public Orderman getOrderman() {
        return orderman;
    }

    public void setOrderman(Orderman orderman) {
        this.orderman = orderman;
    }

    public Waitress getWaitress() {
        return waitress;
    }

    public void setWaitress(Waitress waitress) {
        this.waitress = waitress;
    }

    public Chef getChef() {
        return chef;
    }

    public void setChef(Chef chef) {
        this.chef = chef;
    }

    public Telephonist getTelephonist() {
        return telephonist;
    }

    public void setTelephonist(Telephonist telephonist) {
        this.telephonist = telephonist;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Employee[ id=" + id + " ]";
    }
    
}
