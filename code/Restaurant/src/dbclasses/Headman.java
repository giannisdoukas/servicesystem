/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "headman")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Headman.findAll", query = "SELECT h FROM Headman h"),
    @NamedQuery(name = "Headman.findByHeadId", query = "SELECT h FROM Headman h WHERE h.headId = :headId")})
public class Headman implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "head_id")
    private Integer headId;
    @OneToMany(mappedBy = "headman")
    private Collection<Shift> shiftCollection;
    @JoinColumn(name = "head_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Employee employee;

    public Headman() {
    }

    public Headman(Integer headId) {
        this.headId = headId;
    }

    public Integer getHeadId() {
        return headId;
    }

    public void setHeadId(Integer headId) {
        this.headId = headId;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection() {
        return shiftCollection;
    }

    public void setShiftCollection(Collection<Shift> shiftCollection) {
        this.shiftCollection = shiftCollection;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (headId != null ? headId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Headman)) {
            return false;
        }
        Headman other = (Headman) object;
        if ((this.headId == null && other.headId != null) || (this.headId != null && !this.headId.equals(other.headId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Headman[ headId=" + headId + " ]";
    }
    
}
