/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dks
 */
@Embeddable
public class OrderFromPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "orderClient")
    private int orderClient;
    @Basic(optional = false)
    @Column(name = "orderID")
    private int orderID;
    @Basic(optional = false)
    @Column(name = "orderTable")
    private int orderTable;

    public OrderFromPK() {
    }

    public OrderFromPK(int orderClient, int orderID, int orderTable) {
        this.orderClient = orderClient;
        this.orderID = orderID;
        this.orderTable = orderTable;
    }

    public int getOrderClient() {
        return orderClient;
    }

    public void setOrderClient(int orderClient) {
        this.orderClient = orderClient;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getOrderTable() {
        return orderTable;
    }

    public void setOrderTable(int orderTable) {
        this.orderTable = orderTable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) orderClient;
        hash += (int) orderID;
        hash += (int) orderTable;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderFromPK)) {
            return false;
        }
        OrderFromPK other = (OrderFromPK) object;
        if (this.orderClient != other.orderClient) {
            return false;
        }
        if (this.orderID != other.orderID) {
            return false;
        }
        if (this.orderTable != other.orderTable) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.OrderFromPK[ orderClient=" + orderClient + ", orderID=" + orderID + ", orderTable=" + orderTable + " ]";
    }
    
}
