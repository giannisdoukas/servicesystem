/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "soup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Soup.findAll", query = "SELECT s FROM Soup s"),
    @NamedQuery(name = "Soup.findBySoupName", query = "SELECT s FROM Soup s WHERE s.soupName = :soupName")})
public class Soup implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "soup_name")
    private String soupName;
    @JoinColumn(name = "soup_name", referencedColumnName = "name", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Dish dish;

    public Soup() {
    }

    public Soup(String soupName) {
        this.soupName = soupName;
    }

    public String getSoupName() {
        return soupName;
    }

    public void setSoupName(String soupName) {
        this.soupName = soupName;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (soupName != null ? soupName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Soup)) {
            return false;
        }
        Soup other = (Soup) object;
        if ((this.soupName == null && other.soupName != null) || (this.soupName != null && !this.soupName.equals(other.soupName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Soup[ soupName=" + soupName + " ]";
    }
    
}
