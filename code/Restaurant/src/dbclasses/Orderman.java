/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "orderman")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orderman.findAll", query = "SELECT o FROM Orderman o"),
    @NamedQuery(name = "Orderman.findByOrdId", query = "SELECT o FROM Orderman o WHERE o.ordId = :ordId")})
public class Orderman implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ord_id")
    private Integer ordId;
    @OneToMany(mappedBy = "orderman1")
    private Collection<Shift> shiftCollection;
    @OneToMany(mappedBy = "orderman2")
    private Collection<Shift> shiftCollection1;
    @JoinColumn(name = "ord_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Employee employee;

    public Orderman() {
    }

    public Orderman(Integer ordId) {
        this.ordId = ordId;
    }

    public Integer getOrdId() {
        return ordId;
    }

    public void setOrdId(Integer ordId) {
        this.ordId = ordId;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection() {
        return shiftCollection;
    }

    public void setShiftCollection(Collection<Shift> shiftCollection) {
        this.shiftCollection = shiftCollection;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection1() {
        return shiftCollection1;
    }

    public void setShiftCollection1(Collection<Shift> shiftCollection1) {
        this.shiftCollection1 = shiftCollection1;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ordId != null ? ordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orderman)) {
            return false;
        }
        Orderman other = (Orderman) object;
        if ((this.ordId == null && other.ordId != null) || (this.ordId != null && !this.ordId.equals(other.ordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Orderman[ ordId=" + ordId + " ]";
    }
    
}
