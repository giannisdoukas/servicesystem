/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "waitress")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Waitress.findAll", query = "SELECT w FROM Waitress w"),
    @NamedQuery(name = "Waitress.findByWaitId", query = "SELECT w FROM Waitress w WHERE w.waitId = :waitId")})
public class Waitress implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "wait_id")
    private Integer waitId;
    @OneToMany(mappedBy = "waitress1")
    private Collection<Shift> shiftCollection;
    @OneToMany(mappedBy = "waitress2")
    private Collection<Shift> shiftCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "waitress")
    private Collection<MyTable> myTableCollection;
    @JoinColumn(name = "wait_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Employee employee;

    public Waitress() {
    }

    public Waitress(Integer waitId) {
        this.waitId = waitId;
    }

    public Integer getWaitId() {
        return waitId;
    }

    public void setWaitId(Integer waitId) {
        this.waitId = waitId;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection() {
        return shiftCollection;
    }

    public void setShiftCollection(Collection<Shift> shiftCollection) {
        this.shiftCollection = shiftCollection;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection1() {
        return shiftCollection1;
    }

    public void setShiftCollection1(Collection<Shift> shiftCollection1) {
        this.shiftCollection1 = shiftCollection1;
    }

    @XmlTransient
    public Collection<MyTable> getMyTableCollection() {
        return myTableCollection;
    }

    public void setMyTableCollection(Collection<MyTable> myTableCollection) {
        this.myTableCollection = myTableCollection;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (waitId != null ? waitId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Waitress)) {
            return false;
        }
        Waitress other = (Waitress) object;
        if ((this.waitId == null && other.waitId != null) || (this.waitId != null && !this.waitId.equals(other.waitId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Waitress[ waitId=" + waitId + " ]";
    }
    
}
