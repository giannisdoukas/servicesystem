/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "ingredients")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ingredients.findAll", query = "SELECT i FROM Ingredients i"),
    @NamedQuery(name = "Ingredients.findByName", query = "SELECT i FROM Ingredients i WHERE i.name = :name"),
    @NamedQuery(name = "Ingredients.findByAvailableQuantity", query = "SELECT i FROM Ingredients i WHERE i.availableQuantity = :availableQuantity"),
    @NamedQuery(name = "Ingredients.findByQuantityBound", query = "SELECT i FROM Ingredients i WHERE i.quantityBound = :quantityBound")})
public class Ingredients implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "available_quantity")
    private int availableQuantity;
    @Column(name = "quantity_bound")
    private Integer quantityBound;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ingredients1")
    private Collection<Contains> containsCollection;

    public Ingredients() {
    }

    public Ingredients(String name) {
        this.name = name;
    }

    public Ingredients(String name, int availableQuantity) {
        this.name = name;
        this.availableQuantity = availableQuantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public Integer getQuantityBound() {
        return quantityBound;
    }

    public void setQuantityBound(Integer quantityBound) {
        this.quantityBound = quantityBound;
    }

    @XmlTransient
    public Collection<Contains> getContainsCollection() {
        return containsCollection;
    }

    public void setContainsCollection(Collection<Contains> containsCollection) {
        this.containsCollection = containsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ingredients)) {
            return false;
        }
        Ingredients other = (Ingredients) object;
        if ((this.name == null && other.name != null) || (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "dbclasses.Ingredients[ name=" + name + " ]";
        return name;
    }
    
}
