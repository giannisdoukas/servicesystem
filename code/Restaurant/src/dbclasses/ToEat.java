/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "toEat")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ToEat.findAll", query = "SELECT t FROM ToEat t"),
    @NamedQuery(name = "ToEat.findByToEatID", query = "SELECT t FROM ToEat t WHERE t.toEatID = :toEatID")})
public class ToEat implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "toEatID")
    private Integer toEatID;
    @JoinColumn(name = "dishToEat", referencedColumnName = "name")
    @ManyToOne(optional = false)
    private Dish dishToEat;
    @JoinColumn(name = "orderToEat", referencedColumnName = "idOrder")
    @ManyToOne(optional = false)
    private MyOrder orderToEat;

    public ToEat() {
    }

    public ToEat(Integer toEatID) {
        this.toEatID = toEatID;
    }

    public Integer getToEatID() {
        return toEatID;
    }

    public void setToEatID(Integer toEatID) {
        this.toEatID = toEatID;
    }

    public Dish getDishToEat() {
        return dishToEat;
    }

    public void setDishToEat(Dish dishToEat) {
        this.dishToEat = dishToEat;
    }

    public MyOrder getOrderToEat() {
        return orderToEat;
    }

    public void setOrderToEat(MyOrder orderToEat) {
        this.orderToEat = orderToEat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (toEatID != null ? toEatID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ToEat)) {
            return false;
        }
        ToEat other = (ToEat) object;
        if ((this.toEatID == null && other.toEatID != null) || (this.toEatID != null && !this.toEatID.equals(other.toEatID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.ToEat[ toEatID=" + toEatID + " ]";
    }
    
}
