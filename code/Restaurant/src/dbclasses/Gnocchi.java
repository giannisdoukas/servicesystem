/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "gnocchi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gnocchi.findAll", query = "SELECT g FROM Gnocchi g"),
    @NamedQuery(name = "Gnocchi.findByGnocchiName", query = "SELECT g FROM Gnocchi g WHERE g.gnocchiName = :gnocchiName")})
public class Gnocchi implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "gnocchi_name")
    private String gnocchiName;
    @JoinColumn(name = "gnocchi_name", referencedColumnName = "main_name", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private MainDish mainDish;

    public Gnocchi() {
    }

    public Gnocchi(String gnocchiName) {
        this.gnocchiName = gnocchiName;
    }

    public String getGnocchiName() {
        return gnocchiName;
    }

    public void setGnocchiName(String gnocchiName) {
        this.gnocchiName = gnocchiName;
    }

    public MainDish getMainDish() {
        return mainDish;
    }

    public void setMainDish(MainDish mainDish) {
        this.mainDish = mainDish;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gnocchiName != null ? gnocchiName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gnocchi)) {
            return false;
        }
        Gnocchi other = (Gnocchi) object;
        if ((this.gnocchiName == null && other.gnocchiName != null) || (this.gnocchiName != null && !this.gnocchiName.equals(other.gnocchiName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Gnocchi[ gnocchiName=" + gnocchiName + " ]";
    }
    
}
