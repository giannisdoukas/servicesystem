/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "risotto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Risotto.findAll", query = "SELECT r FROM Risotto r"),
    @NamedQuery(name = "Risotto.findByRisotoName", query = "SELECT r FROM Risotto r WHERE r.risotoName = :risotoName")})
public class Risotto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "risoto_name")
    private String risotoName;
    @JoinColumn(name = "risoto_name", referencedColumnName = "main_name", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private MainDish mainDish;

    public Risotto() {
    }

    public Risotto(String risotoName) {
        this.risotoName = risotoName;
    }

    public String getRisotoName() {
        return risotoName;
    }

    public void setRisotoName(String risotoName) {
        this.risotoName = risotoName;
    }

    public MainDish getMainDish() {
        return mainDish;
    }

    public void setMainDish(MainDish mainDish) {
        this.mainDish = mainDish;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (risotoName != null ? risotoName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Risotto)) {
            return false;
        }
        Risotto other = (Risotto) object;
        if ((this.risotoName == null && other.risotoName != null) || (this.risotoName != null && !this.risotoName.equals(other.risotoName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Risotto[ risotoName=" + risotoName + " ]";
    }
    
}
