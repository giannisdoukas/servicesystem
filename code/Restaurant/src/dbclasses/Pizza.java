/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "pizza")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pizza.findAll", query = "SELECT p FROM Pizza p"),
    @NamedQuery(name = "Pizza.findByPizzaName", query = "SELECT p FROM Pizza p WHERE p.pizzaName = :pizzaName"),
    @NamedQuery(name = "Pizza.findBySize", query = "SELECT p FROM Pizza p WHERE p.size = :size")})
public class Pizza implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "pizza_name")
    private String pizzaName;
    @Basic(optional = false)
    @Column(name = "size")
    private String size;
    @JoinColumn(name = "pizza_name", referencedColumnName = "main_name", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private MainDish mainDish;

    public Pizza() {
    }

    public Pizza(String pizzaName) {
        this.pizzaName = pizzaName;
    }

    public Pizza(String pizzaName, String size) {
        this.pizzaName = pizzaName;
        this.size = size;
    }

    public String getPizzaName() {
        return pizzaName;
    }

    public void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public MainDish getMainDish() {
        return mainDish;
    }

    public void setMainDish(MainDish mainDish) {
        this.mainDish = mainDish;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pizzaName != null ? pizzaName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pizza)) {
            return false;
        }
        Pizza other = (Pizza) object;
        if ((this.pizzaName == null && other.pizzaName != null) || (this.pizzaName != null && !this.pizzaName.equals(other.pizzaName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Pizza[ pizzaName=" + pizzaName + " ]";
    }
    
}
