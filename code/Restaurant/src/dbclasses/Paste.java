/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "paste")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Paste.findAll", query = "SELECT p FROM Paste p"),
    @NamedQuery(name = "Paste.findByPasteName", query = "SELECT p FROM Paste p WHERE p.pastePK.pasteName = :pasteName"),
    @NamedQuery(name = "Paste.findByPasteKind", query = "SELECT p FROM Paste p WHERE p.pastePK.pasteKind = :pasteKind")})
public class Paste implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PastePK pastePK;
    @JoinColumn(name = "paste_name", referencedColumnName = "main_name", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MainDish mainDish;

    public Paste() {
    }

    public Paste(PastePK pastePK) {
        this.pastePK = pastePK;
    }

    public Paste(String pasteName, String pasteKind) {
        this.pastePK = new PastePK(pasteName, pasteKind);
    }

    public PastePK getPastePK() {
        return pastePK;
    }

    public void setPastePK(PastePK pastePK) {
        this.pastePK = pastePK;
    }

    public MainDish getMainDish() {
        return mainDish;
    }

    public void setMainDish(MainDish mainDish) {
        this.mainDish = mainDish;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pastePK != null ? pastePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paste)) {
            return false;
        }
        Paste other = (Paste) object;
        if ((this.pastePK == null && other.pastePK != null) || (this.pastePK != null && !this.pastePK.equals(other.pastePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Paste[ pastePK=" + pastePK + " ]";
    }
    
}
