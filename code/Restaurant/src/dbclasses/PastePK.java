/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dks
 */
@Embeddable
public class PastePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "paste_name")
    private String pasteName;
    @Basic(optional = false)
    @Column(name = "paste_kind")
    private String pasteKind;

    public PastePK() {
    }

    public PastePK(String pasteName, String pasteKind) {
        this.pasteName = pasteName;
        this.pasteKind = pasteKind;
    }

    public String getPasteName() {
        return pasteName;
    }

    public void setPasteName(String pasteName) {
        this.pasteName = pasteName;
    }

    public String getPasteKind() {
        return pasteKind;
    }

    public void setPasteKind(String pasteKind) {
        this.pasteKind = pasteKind;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pasteName != null ? pasteName.hashCode() : 0);
        hash += (pasteKind != null ? pasteKind.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PastePK)) {
            return false;
        }
        PastePK other = (PastePK) object;
        if ((this.pasteName == null && other.pasteName != null) || (this.pasteName != null && !this.pasteName.equals(other.pasteName))) {
            return false;
        }
        if ((this.pasteKind == null && other.pasteKind != null) || (this.pasteKind != null && !this.pasteKind.equals(other.pasteKind))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.PastePK[ pasteName=" + pasteName + ", pasteKind=" + pasteKind + " ]";
    }
    
}
