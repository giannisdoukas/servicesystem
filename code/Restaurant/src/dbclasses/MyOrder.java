/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "myOrder")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MyOrder.findAll", query = "SELECT m FROM MyOrder m"),
    @NamedQuery(name = "MyOrder.findByIdOrder", query = "SELECT m FROM MyOrder m WHERE m.idOrder = :idOrder"),
    @NamedQuery(name = "MyOrder.findByDate", query = "SELECT m FROM MyOrder m WHERE m.date = :date"),
    @NamedQuery(name = "MyOrder.findByDone", query = "SELECT m FROM MyOrder m WHERE m.done = :done"),
    @NamedQuery(name = "MyOrder.findByPaid", query = "SELECT m FROM MyOrder m WHERE m.paid = :paid")})
public class MyOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idOrder")
    private Integer idOrder;
    @Basic(optional = false)
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Basic(optional = false)
    @Column(name = "done")
    private boolean done;
    @Basic(optional = false)
    @Column(name = "paid")
    private boolean paid;

    public MyOrder() {
    }

    public MyOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }
    
    public MyOrder(Integer idOrder, Date date, boolean done) {
        this.idOrder = idOrder;
        this.date = date;
        this.done = done;
    }
    
    public MyOrder(Integer idOrder, Date date, boolean done, boolean paid) {
        this.idOrder = idOrder;
        this.date = date;
        this.done = done;
        this.paid = paid;
    }

    public Integer getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean getDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean getPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOrder != null ? idOrder.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MyOrder)) {
            return false;
        }
        MyOrder other = (MyOrder) object;
        if ((this.idOrder == null && other.idOrder != null) || (this.idOrder != null && !this.idOrder.equals(other.idOrder))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return date + "\t" + idOrder;
    }
    
}
