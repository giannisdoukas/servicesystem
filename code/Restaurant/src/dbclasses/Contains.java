/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "contains")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contains.findAll", query = "SELECT c FROM Contains c"),
    @NamedQuery(name = "Contains.findByDish", query = "SELECT c FROM Contains c WHERE c.containsPK.dish = :dish"),
    @NamedQuery(name = "Contains.findByQuantity", query = "SELECT c FROM Contains c WHERE c.quantity = :quantity"),
    @NamedQuery(name = "Contains.findByIngredients", query = "SELECT c FROM Contains c WHERE c.containsPK.ingredients = :ingredients")})
public class Contains implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContainsPK containsPK;
    @Column(name = "quantity")
    private Integer quantity;
    @JoinColumn(name = "dish", referencedColumnName = "name", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Dish dish1;
    @JoinColumn(name = "ingredients", referencedColumnName = "name", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ingredients ingredients1;

    public Contains() {
    }

    public Contains(ContainsPK containsPK) {
        this.containsPK = containsPK;
    }

    public Contains(String dish, String ingredients) {
        this.containsPK = new ContainsPK(dish, ingredients);
    }

    public Contains(Ingredients ingredient, Integer quantity){
        this.ingredients1 = ingredient;
        this.quantity = quantity;
    }
    
    public ContainsPK getContainsPK() {
        return containsPK;
    }

    public void setContainsPK(ContainsPK containsPK) {
        this.containsPK = containsPK;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Dish getDish1() {
        return dish1;
    }

    public void setDish1(Dish dish1) {
        this.dish1 = dish1;
    }

    public Ingredients getIngredients1() {
        return ingredients1;
    }

    public void setIngredients1(Ingredients ingredients1) {
        this.ingredients1 = ingredients1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (containsPK != null ? containsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contains)) {
            return false;
        }
        Contains other = (Contains) object;
        if ((this.containsPK == null && other.containsPK != null) || (this.containsPK != null && !this.containsPK.equals(other.containsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "dbclasses.Contains[ containsPK=" + containsPK + " ]";
        return ingredients1.getName() + "(x" + quantity + ")";
    }
    
}
