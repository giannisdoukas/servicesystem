/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "drinks")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Drinks.findAll", query = "SELECT d FROM Drinks d"),
    @NamedQuery(name = "Drinks.findByName", query = "SELECT d FROM Drinks d WHERE d.name = :name"),
    @NamedQuery(name = "Drinks.findByAvailableQuantity", query = "SELECT d FROM Drinks d WHERE d.availableQuantity = :availableQuantity"),
    @NamedQuery(name = "Drinks.findByQuantityBound", query = "SELECT d FROM Drinks d WHERE d.quantityBound = :quantityBound"),
    @NamedQuery(name = "Drinks.findByPrice", query = "SELECT d FROM Drinks d WHERE d.price = :price")})
public class Drinks implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "available_quantity")
    private Integer availableQuantity;
    @Column(name = "quantity_bound")
    private Integer quantityBound;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "price")
    private BigDecimal price;
    @JoinTable(name = "toDrink", joinColumns = {
        @JoinColumn(name = "drinkToDrink", referencedColumnName = "name")}, inverseJoinColumns = {
        @JoinColumn(name = "orderToDrink", referencedColumnName = "idOrder")})
    @ManyToMany
    private Collection<MyOrder> myOrderCollection;

    public Drinks() {
    }

    public Drinks(String name) {
        this.name = name;
    }

    public Drinks(String name, BigDecimal price){
        this.name = name;
        this.price = price;
    }
    
    public Drinks(String name, int availableQuantity, BigDecimal price) {
        this.name = name;
        this.price = price;
        this.availableQuantity = availableQuantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(Integer availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public Integer getQuantityBound() {
        return quantityBound;
    }

    public void setQuantityBound(Integer quantityBound) {
        this.quantityBound = quantityBound;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    
    public Dish toDish(){
        Dish dish = new Dish();
        dish.setName(this.name);
        dish.setPrice(this.price);
        return dish;
    }
    

    @XmlTransient
    public Collection<MyOrder> getMyOrderCollection() {
        return myOrderCollection;
    }

    public void setMyOrderCollection(Collection<MyOrder> myOrderCollection) {
        this.myOrderCollection = myOrderCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Drinks)) {
            return false;
        }
        Drinks other = (Drinks) object;
        if ((this.name == null && other.name != null) || (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Drinks[ name=" + name + " ]";
    }
    
}
