/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "dish")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dish.findAll", query = "SELECT d FROM Dish d"),
    @NamedQuery(name = "Dish.findByName", query = "SELECT d FROM Dish d WHERE d.name = :name"),
    @NamedQuery(name = "Dish.findByTime", query = "SELECT d FROM Dish d WHERE d.time = :time"),
    @NamedQuery(name = "Dish.findByPrice", query = "SELECT d FROM Dish d WHERE d.price = :price")})
public class Dish implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "time")
    private int time;
    @Lob
    @Column(name = "instructions")
    private String instructions;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "price")
    private BigDecimal price;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "dish")
    private Soup soup;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "dish")
    private MainDish mainDish;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dishToEat")
    private Collection<ToEat> toEatCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "dish")
    private Salat salat;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "dish")
    private TodaysSpecial todaysSpecial;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dish1")
    private Collection<Contains> containsCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "dish")
    private Appetizer appetizer;

    public Dish() {
    }

    public Dish(String name) {
        this.name = name;
    }

    public Dish(String name, int time, String instructions, BigDecimal price) {
        this.name = name;
        this.time = time;
        this.price = price;
        this.instructions = instructions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Soup getSoup() {
        return soup;
    }

    public void setSoup(Soup soup) {
        this.soup = soup;
    }

    public MainDish getMainDish() {
        return mainDish;
    }

    public void setMainDish(MainDish mainDish) {
        this.mainDish = mainDish;
    }

    @XmlTransient
    public Collection<ToEat> getToEatCollection() {
        return toEatCollection;
    }

    public void setToEatCollection(Collection<ToEat> toEatCollection) {
        this.toEatCollection = toEatCollection;
    }

    public Salat getSalat() {
        return salat;
    }

    public void setSalat(Salat salat) {
        this.salat = salat;
    }

    public TodaysSpecial getTodaysSpecial() {
        return todaysSpecial;
    }

    public void setTodaysSpecial(TodaysSpecial todaysSpecial) {
        this.todaysSpecial = todaysSpecial;
    }

    @XmlTransient
    public Collection<Contains> getContainsCollection() {
        return containsCollection;
    }

    public void setContainsCollection(Collection<Contains> containsCollection) {
        this.containsCollection = containsCollection;
    }

    public Appetizer getAppetizer() {
        return appetizer;
    }

    public void setAppetizer(Appetizer appetizer) {
        this.appetizer = appetizer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dish)) {
            return false;
        }
        Dish other = (Dish) object;
        if ((this.name == null && other.name != null) || (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Dish[ name=" + name + " ]";
    }
    
}
