/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "appetizer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Appetizer.findAll", query = "SELECT a FROM Appetizer a"),
    @NamedQuery(name = "Appetizer.findByAppetizerName", query = "SELECT a FROM Appetizer a WHERE a.appetizerName = :appetizerName")})
public class Appetizer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "appetizer_name")
    private String appetizerName;
    @JoinColumn(name = "appetizer_name", referencedColumnName = "name", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Dish dish;

    public Appetizer() {
    }

    public Appetizer(String appetizerName) {
        this.appetizerName = appetizerName;
    }

    public String getAppetizerName() {
        return appetizerName;
    }

    public void setAppetizerName(String appetizerName) {
        this.appetizerName = appetizerName;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (appetizerName != null ? appetizerName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Appetizer)) {
            return false;
        }
        Appetizer other = (Appetizer) object;
        if ((this.appetizerName == null && other.appetizerName != null) || (this.appetizerName != null && !this.appetizerName.equals(other.appetizerName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Appetizer[ appetizerName=" + appetizerName + " ]";
    }
    
}
