/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "todays_special")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TodaysSpecial.findAll", query = "SELECT t FROM TodaysSpecial t"),
    @NamedQuery(name = "TodaysSpecial.findBySpecialName", query = "SELECT t FROM TodaysSpecial t WHERE t.specialName = :specialName"),
    @NamedQuery(name = "TodaysSpecial.findByAvailable", query = "SELECT t FROM TodaysSpecial t WHERE t.available = :available")})
public class TodaysSpecial implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "special_name")
    private String specialName;
    @Basic(optional = false)
    @Column(name = "available")
    private int available;
    @JoinColumn(name = "special_name", referencedColumnName = "name", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Dish dish;

    public TodaysSpecial() {
    }

    public TodaysSpecial(String specialName) {
        this.specialName = specialName;
    }

    public TodaysSpecial(String specialName, int available) {
        this.specialName = specialName;
        this.available = available;
    }

    public String getSpecialName() {
        return specialName;
    }

    public void setSpecialName(String specialName) {
        this.specialName = specialName;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (specialName != null ? specialName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TodaysSpecial)) {
            return false;
        }
        TodaysSpecial other = (TodaysSpecial) object;
        if ((this.specialName == null && other.specialName != null) || (this.specialName != null && !this.specialName.equals(other.specialName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.TodaysSpecial[ specialName=" + specialName + " ]";
    }
    
}
