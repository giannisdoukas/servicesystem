/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "mainDish")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MainDish.findAll", query = "SELECT m FROM MainDish m"),
    @NamedQuery(name = "MainDish.findByMainName", query = "SELECT m FROM MainDish m WHERE m.mainName = :mainName")})
public class MainDish implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "main_name")
    private String mainName;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "mainDish")
    private Risotto risotto;
    @JoinColumn(name = "main_name", referencedColumnName = "name", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Dish dish;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mainDish")
    private Collection<Paste> pasteCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "mainDish")
    private Pizza pizza;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "mainDish")
    private Gnocchi gnocchi;

    public MainDish() {
    }

    public MainDish(String mainName) {
        this.mainName = mainName;
    }

    public String getMainName() {
        return mainName;
    }

    public void setMainName(String mainName) {
        this.mainName = mainName;
    }

    public Risotto getRisotto() {
        return risotto;
    }

    public void setRisotto(Risotto risotto) {
        this.risotto = risotto;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    @XmlTransient
    public Collection<Paste> getPasteCollection() {
        return pasteCollection;
    }

    public void setPasteCollection(Collection<Paste> pasteCollection) {
        this.pasteCollection = pasteCollection;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public Gnocchi getGnocchi() {
        return gnocchi;
    }

    public void setGnocchi(Gnocchi gnocchi) {
        this.gnocchi = gnocchi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mainName != null ? mainName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MainDish)) {
            return false;
        }
        MainDish other = (MainDish) object;
        if ((this.mainName == null && other.mainName != null) || (this.mainName != null && !this.mainName.equals(other.mainName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.MainDish[ mainName=" + mainName + " ]";
    }
    
}
