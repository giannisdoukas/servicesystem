/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "telephonist")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Telephonist.findAll", query = "SELECT t FROM Telephonist t"),
    @NamedQuery(name = "Telephonist.findByTelId", query = "SELECT t FROM Telephonist t WHERE t.telId = :telId")})
public class Telephonist implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "tel_id")
    private Integer telId;
    @OneToMany(mappedBy = "telephonist")
    private Collection<Shift> shiftCollection;
    @JoinColumn(name = "tel_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Employee employee;

    public Telephonist() {
    }

    public Telephonist(Integer telId) {
        this.telId = telId;
    }

    public Integer getTelId() {
        return telId;
    }

    public void setTelId(Integer telId) {
        this.telId = telId;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection() {
        return shiftCollection;
    }

    public void setShiftCollection(Collection<Shift> shiftCollection) {
        this.shiftCollection = shiftCollection;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (telId != null ? telId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Telephonist)) {
            return false;
        }
        Telephonist other = (Telephonist) object;
        if ((this.telId == null && other.telId != null) || (this.telId != null && !this.telId.equals(other.telId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Telephonist[ telId=" + telId + " ]";
    }
    
}
