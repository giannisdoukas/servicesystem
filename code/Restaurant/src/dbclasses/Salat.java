/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "salat")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Salat.findAll", query = "SELECT s FROM Salat s"),
    @NamedQuery(name = "Salat.findBySalatName", query = "SELECT s FROM Salat s WHERE s.salatName = :salatName")})
public class Salat implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "salat_name")
    private String salatName;
    @JoinColumn(name = "salat_name", referencedColumnName = "name", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Dish dish;

    public Salat() {
    }

    public Salat(String salatName) {
        this.salatName = salatName;
    }

    public String getSalatName() {
        return salatName;
    }

    public void setSalatName(String salatName) {
        this.salatName = salatName;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (salatName != null ? salatName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Salat)) {
            return false;
        }
        Salat other = (Salat) object;
        if ((this.salatName == null && other.salatName != null) || (this.salatName != null && !this.salatName.equals(other.salatName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Salat[ salatName=" + salatName + " ]";
    }
    
}
