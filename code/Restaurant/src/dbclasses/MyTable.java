/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "myTable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MyTable.findAll", query = "SELECT m FROM MyTable m"),
    @NamedQuery(name = "MyTable.findByNumber", query = "SELECT m FROM MyTable m WHERE m.number = :number"),
    @NamedQuery(name = "MyTable.findByCapacity", query = "SELECT m FROM MyTable m WHERE m.capacity = :capacity"),
    @NamedQuery(name = "MyTable.findByDescription", query = "SELECT m FROM MyTable m WHERE m.description = :description")})
public class MyTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "number")
    private Integer number;
    @Basic(optional = false)
    @Column(name = "capacity")
    private Integer capacity;
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "waitress", referencedColumnName = "wait_id")
    @ManyToOne(optional = false)
    private Waitress waitress;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "myTable")
    private Collection<OrderFrom> orderFromCollection;

    public MyTable() {
    }

    public MyTable(Integer number) {
        this.number = number;
    }

    public MyTable(Integer number, Integer capacity) {
        this.number = number;
        this.capacity = capacity;
    }
    
    public MyTable(Integer number, Integer capacity, String description){
        this.number = number;
        this.capacity = capacity;
        this.description = description;
    }
    
    public MyTable(Integer number, Integer capacity, String description, Waitress waitress) {
        this.number = number;
        this.capacity = capacity;
        this.description = description;
        this.waitress = waitress;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Waitress getWaitress() {
        return waitress;
    }

    public void setWaitress(Waitress waitress) {
        this.waitress = waitress;
    }

    @XmlTransient
    public Collection<OrderFrom> getOrderFromCollection() {
        return orderFromCollection;
    }

    public void setOrderFromCollection(Collection<OrderFrom> orderFromCollection) {
        this.orderFromCollection = orderFromCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (number != null ? number.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MyTable)) {
            return false;
        }
        MyTable other = (MyTable) object;
        if ((this.number == null && other.number != null) || (this.number != null && !this.number.equals(other.number))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return number + ": " + description + " (" + capacity + ")";
    }
    
}
