/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "client")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c"),
    @NamedQuery(name = "Client.findByCardId", query = "SELECT c FROM Client c WHERE c.cardId = :cardId"),
    @NamedQuery(name = "Client.findByName", query = "SELECT c FROM Client c WHERE c.name = :name"),
    @NamedQuery(name = "Client.findBySurname", query = "SELECT c FROM Client c WHERE c.surname = :surname"),
    @NamedQuery(name = "Client.findByPoints", query = "SELECT c FROM Client c WHERE c.points = :points"),
    @NamedQuery(name = "Client.findByNumOfOrders", query = "SELECT c FROM Client c WHERE c.numOfOrders = :numOfOrders")})
public class Client implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "card_id")
    private Integer cardId;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Basic(optional = false)
    @Column(name = "points")
    private int points;
    @Basic(optional = false)
    @Column(name = "num_of_orders")
    private int numOfOrders;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
    private Collection<OrderFrom> orderFromCollection;

    public Client() {
    }

    public Client(Integer cardId) {
        this.cardId = cardId;
    }

    public Client(Integer cardId, int points, int numOfOrders) {
        this.cardId = cardId;
        this.points = points;
        this.numOfOrders = numOfOrders;
    }
    
    public Client(Integer cardId, String surname, String name){
        this.cardId = cardId;
        this.surname = surname;
        this.name = name;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getNumOfOrders() {
        return numOfOrders;
    }

    public void setNumOfOrders(int numOfOrders) {
        this.numOfOrders = numOfOrders;
    }

    @XmlTransient
    public Collection<OrderFrom> getOrderFromCollection() {
        return orderFromCollection;
    }

    public void setOrderFromCollection(Collection<OrderFrom> orderFromCollection) {
        this.orderFromCollection = orderFromCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardId != null ? cardId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.cardId == null && other.cardId != null) || (this.cardId != null && !this.cardId.equals(other.cardId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "dbclasses.Client[ cardId=" + cardId + " ]";
        return cardId + "\t" + surname + " " + name;
    }
    
}
