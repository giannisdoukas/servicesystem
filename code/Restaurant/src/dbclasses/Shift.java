/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "shift")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Shift.findAll", query = "SELECT s FROM Shift s"),
    @NamedQuery(name = "Shift.findByDate", query = "SELECT s FROM Shift s WHERE s.shiftPK.date = :date"),
    @NamedQuery(name = "Shift.findBySTime", query = "SELECT s FROM Shift s WHERE s.shiftPK.sTime = :sTime")})
public class Shift implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ShiftPK shiftPK;
    @JoinColumn(name = "chef1", referencedColumnName = "chef_id")
    @ManyToOne
    private Chef chef1;
    @JoinColumn(name = "cashier", referencedColumnName = "cash_id")
    @ManyToOne
    private Cashier cashier;
    @JoinColumn(name = "chef2", referencedColumnName = "chef_id")
    @ManyToOne
    private Chef chef2;
    @JoinColumn(name = "chef3", referencedColumnName = "chef_id")
    @ManyToOne
    private Chef chef3;
    @JoinColumn(name = "headman", referencedColumnName = "head_id")
    @ManyToOne
    private Headman headman;
    @JoinColumn(name = "maid1", referencedColumnName = "maid_id")
    @ManyToOne
    private Maid maid1;
    @JoinColumn(name = "maid2", referencedColumnName = "maid_id")
    @ManyToOne
    private Maid maid2;
    @JoinColumn(name = "orderman1", referencedColumnName = "ord_id")
    @ManyToOne
    private Orderman orderman1;
    @JoinColumn(name = "orderman2", referencedColumnName = "ord_id")
    @ManyToOne
    private Orderman orderman2;
    @JoinColumn(name = "telephonist", referencedColumnName = "tel_id")
    @ManyToOne
    private Telephonist telephonist;
    @JoinColumn(name = "waitress1", referencedColumnName = "wait_id")
    @ManyToOne
    private Waitress waitress1;
    @JoinColumn(name = "waitress2", referencedColumnName = "wait_id")
    @ManyToOne
    private Waitress waitress2;

    public Shift() {
    }

    public Shift(ShiftPK shiftPK) {
        this.shiftPK = shiftPK;
    }

    public Shift(Date date, String sTime) {
        this.shiftPK = new ShiftPK(date, sTime);
    }

    public ShiftPK getShiftPK() {
        return shiftPK;
    }

    public void setShiftPK(ShiftPK shiftPK) {
        this.shiftPK = shiftPK;
    }

    public Chef getChef1() {
        return chef1;
    }

    public void setChef1(Chef chef1) {
        this.chef1 = chef1;
    }

    public Cashier getCashier() {
        return cashier;
    }

    public void setCashier(Cashier cashier) {
        this.cashier = cashier;
    }

    public Chef getChef2() {
        return chef2;
    }

    public void setChef2(Chef chef2) {
        this.chef2 = chef2;
    }

    public Chef getChef3() {
        return chef3;
    }

    public void setChef3(Chef chef3) {
        this.chef3 = chef3;
    }

    public Headman getHeadman() {
        return headman;
    }

    public void setHeadman(Headman headman) {
        this.headman = headman;
    }

    public Maid getMaid1() {
        return maid1;
    }

    public void setMaid1(Maid maid1) {
        this.maid1 = maid1;
    }

    public Maid getMaid2() {
        return maid2;
    }

    public void setMaid2(Maid maid2) {
        this.maid2 = maid2;
    }

    public Orderman getOrderman1() {
        return orderman1;
    }

    public void setOrderman1(Orderman orderman1) {
        this.orderman1 = orderman1;
    }

    public Orderman getOrderman2() {
        return orderman2;
    }

    public void setOrderman2(Orderman orderman2) {
        this.orderman2 = orderman2;
    }

    public Telephonist getTelephonist() {
        return telephonist;
    }

    public void setTelephonist(Telephonist telephonist) {
        this.telephonist = telephonist;
    }

    public Waitress getWaitress1() {
        return waitress1;
    }

    public void setWaitress1(Waitress waitress1) {
        this.waitress1 = waitress1;
    }

    public Waitress getWaitress2() {
        return waitress2;
    }

    public void setWaitress2(Waitress waitress2) {
        this.waitress2 = waitress2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shiftPK != null ? shiftPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Shift)) {
            return false;
        }
        Shift other = (Shift) object;
        if ((this.shiftPK == null && other.shiftPK != null) || (this.shiftPK != null && !this.shiftPK.equals(other.shiftPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Shift[ shiftPK=" + shiftPK + " ]";
    }
    
}
