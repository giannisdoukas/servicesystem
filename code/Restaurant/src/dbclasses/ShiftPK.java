/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author dks
 */
@Embeddable
public class ShiftPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "date")
    @Temporal(TemporalType.TIME)
    private Date date;
    @Basic(optional = false)
    @Column(name = "sTime")
    private String sTime;

    public ShiftPK() {
    }

    public ShiftPK(Date date, String sTime) {
        this.date = date;
        this.sTime = sTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSTime() {
        return sTime;
    }

    public void setSTime(String sTime) {
        this.sTime = sTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (date != null ? date.hashCode() : 0);
        hash += (sTime != null ? sTime.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ShiftPK)) {
            return false;
        }
        ShiftPK other = (ShiftPK) object;
        if ((this.date == null && other.date != null) || (this.date != null && !this.date.equals(other.date))) {
            return false;
        }
        if ((this.sTime == null && other.sTime != null) || (this.sTime != null && !this.sTime.equals(other.sTime))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.ShiftPK[ date=" + date + ", sTime=" + sTime + " ]";
    }
    
}
