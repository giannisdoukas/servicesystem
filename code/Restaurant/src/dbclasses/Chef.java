/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "chef")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Chef.findAll", query = "SELECT c FROM Chef c"),
    @NamedQuery(name = "Chef.findByChefId", query = "SELECT c FROM Chef c WHERE c.chefId = :chefId")})
public class Chef implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "chef_id")
    private Integer chefId;
    @OneToMany(mappedBy = "chef1")
    private Collection<Shift> shiftCollection;
    @OneToMany(mappedBy = "chef2")
    private Collection<Shift> shiftCollection1;
    @OneToMany(mappedBy = "chef3")
    private Collection<Shift> shiftCollection2;
    @JoinColumn(name = "chef_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Employee employee;

    public Chef() {
    }

    public Chef(Integer chefId) {
        this.chefId = chefId;
    }

    public Integer getChefId() {
        return chefId;
    }

    public void setChefId(Integer chefId) {
        this.chefId = chefId;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection() {
        return shiftCollection;
    }

    public void setShiftCollection(Collection<Shift> shiftCollection) {
        this.shiftCollection = shiftCollection;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection1() {
        return shiftCollection1;
    }

    public void setShiftCollection1(Collection<Shift> shiftCollection1) {
        this.shiftCollection1 = shiftCollection1;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection2() {
        return shiftCollection2;
    }

    public void setShiftCollection2(Collection<Shift> shiftCollection2) {
        this.shiftCollection2 = shiftCollection2;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (chefId != null ? chefId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Chef)) {
            return false;
        }
        Chef other = (Chef) object;
        if ((this.chefId == null && other.chefId != null) || (this.chefId != null && !this.chefId.equals(other.chefId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Chef[ chefId=" + chefId + " ]";
    }
    
}
