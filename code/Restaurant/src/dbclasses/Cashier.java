/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "cashier")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cashier.findAll", query = "SELECT c FROM Cashier c"),
    @NamedQuery(name = "Cashier.findByCashId", query = "SELECT c FROM Cashier c WHERE c.cashId = :cashId")})
public class Cashier implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cash_id")
    private Integer cashId;
    @OneToMany(mappedBy = "cashier")
    private Collection<Shift> shiftCollection;
    @JoinColumn(name = "cash_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Employee employee;

    public Cashier() {
    }

    public Cashier(Integer cashId) {
        this.cashId = cashId;
    }

    public Integer getCashId() {
        return cashId;
    }

    public void setCashId(Integer cashId) {
        this.cashId = cashId;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection() {
        return shiftCollection;
    }

    public void setShiftCollection(Collection<Shift> shiftCollection) {
        this.shiftCollection = shiftCollection;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cashId != null ? cashId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cashier)) {
            return false;
        }
        Cashier other = (Cashier) object;
        if ((this.cashId == null && other.cashId != null) || (this.cashId != null && !this.cashId.equals(other.cashId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Cashier[ cashId=" + cashId + " ]";
    }
    
}
