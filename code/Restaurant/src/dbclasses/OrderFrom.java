/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "orderFrom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderFrom.findAll", query = "SELECT o FROM OrderFrom o"),
    @NamedQuery(name = "OrderFrom.findByOrderClient", query = "SELECT o FROM OrderFrom o WHERE o.orderFromPK.orderClient = :orderClient"),
    @NamedQuery(name = "OrderFrom.findByOrderID", query = "SELECT o FROM OrderFrom o WHERE o.orderFromPK.orderID = :orderID"),
    @NamedQuery(name = "OrderFrom.findByOrderTable", query = "SELECT o FROM OrderFrom o WHERE o.orderFromPK.orderTable = :orderTable")})
public class OrderFrom implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrderFromPK orderFromPK;
    @JoinColumn(name = "orderClient", referencedColumnName = "card_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Client client;
    @JoinColumn(name = "orderID", referencedColumnName = "idOrder", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MyOrder myOrder;
    @JoinColumn(name = "orderTable", referencedColumnName = "number", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MyTable myTable;

    public OrderFrom() {
    }

    public OrderFrom(OrderFromPK orderFromPK) {
        this.orderFromPK = orderFromPK;
    }

    public OrderFrom(int orderClient, int orderID, int orderTable) {
        this.orderFromPK = new OrderFromPK(orderClient, orderID, orderTable);
    }

    public OrderFromPK getOrderFromPK() {
        return orderFromPK;
    }

    public void setOrderFromPK(OrderFromPK orderFromPK) {
        this.orderFromPK = orderFromPK;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public MyOrder getMyOrder() {
        return myOrder;
    }

    public void setMyOrder(MyOrder myOrder) {
        this.myOrder = myOrder;
    }

    public MyTable getMyTable() {
        return myTable;
    }

    public void setMyTable(MyTable myTable) {
        this.myTable = myTable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderFromPK != null ? orderFromPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderFrom)) {
            return false;
        }
        OrderFrom other = (OrderFrom) object;
        if ((this.orderFromPK == null && other.orderFromPK != null) || (this.orderFromPK != null && !this.orderFromPK.equals(other.orderFromPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.OrderFrom[ orderFromPK=" + orderFromPK + " ]";
    }
    
}
