/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dks
 */
@Entity
@Table(name = "maid")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Maid.findAll", query = "SELECT m FROM Maid m"),
    @NamedQuery(name = "Maid.findByMaidId", query = "SELECT m FROM Maid m WHERE m.maidId = :maidId")})
public class Maid implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "maid_id")
    private Integer maidId;
    @OneToMany(mappedBy = "maid1")
    private Collection<Shift> shiftCollection;
    @OneToMany(mappedBy = "maid2")
    private Collection<Shift> shiftCollection1;
    @JoinColumn(name = "maid_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Employee employee;

    public Maid() {
    }

    public Maid(Integer maidId) {
        this.maidId = maidId;
    }

    public Integer getMaidId() {
        return maidId;
    }

    public void setMaidId(Integer maidId) {
        this.maidId = maidId;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection() {
        return shiftCollection;
    }

    public void setShiftCollection(Collection<Shift> shiftCollection) {
        this.shiftCollection = shiftCollection;
    }

    @XmlTransient
    public Collection<Shift> getShiftCollection1() {
        return shiftCollection1;
    }

    public void setShiftCollection1(Collection<Shift> shiftCollection1) {
        this.shiftCollection1 = shiftCollection1;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (maidId != null ? maidId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Maid)) {
            return false;
        }
        Maid other = (Maid) object;
        if ((this.maidId == null && other.maidId != null) || (this.maidId != null && !this.maidId.equals(other.maidId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Maid[ maidId=" + maidId + " ]";
    }
    
}
