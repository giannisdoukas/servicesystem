/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author dks
 */
@Embeddable
public class ContainsPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "dish")
    private String dish;
    @Basic(optional = false)
    @Column(name = "ingredients")
    private String ingredients;

    public ContainsPK() {
    }

    public ContainsPK(String dish, String ingredients) {
        this.dish = dish;
        this.ingredients = ingredients;
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dish != null ? dish.hashCode() : 0);
        hash += (ingredients != null ? ingredients.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContainsPK)) {
            return false;
        }
        ContainsPK other = (ContainsPK) object;
        if ((this.dish == null && other.dish != null) || (this.dish != null && !this.dish.equals(other.dish))) {
            return false;
        }
        if ((this.ingredients == null && other.ingredients != null) || (this.ingredients != null && !this.ingredients.equals(other.ingredients))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.ContainsPK[ dish=" + dish + ", ingredients=" + ingredients + " ]";
    }
    
}
