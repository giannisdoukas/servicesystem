package chef;

import com.mysql.jdbc.Connection;
import dbclasses.Contains;
import dbclasses.Dish;
import dbclasses.Ingredients;
import dbclasses.MyOrder;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.Main;
import waitress.WaitressSceneController;

/**
 * FXML Controller class
 *
 * @author dks
 */
public class ChefSceneController implements Initializable {
    
    @FXML MenuBar menubar;
    
    @FXML VBox vbox;
    
    HBox hbox = new HBox(15);
    Button readyButton = new Button("Ready");
    Button showInfo = new Button("Show informations");
    
    final ComboBox comboBox = new ComboBox();
    private final ObservableList<MyOrder> comboItems = FXCollections.observableArrayList();
    
    final TableView orders = new TableView();
    private final ObservableList<Dish> orderItems = FXCollections.observableArrayList();
    
    int id;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setupButtons();
        ConstructOrdersTable();
        fillComboBox();                       
        
        hbox.getChildren().add(comboBox);
        hbox.getChildren().add(showInfo);
        hbox.getChildren().add(readyButton);
        vbox.getChildren().add(hbox);
        
        vbox.getChildren().add(orders);
    }    
    
    private void ConstructOrdersTable(){
        TableColumn column = new TableColumn<>("Name");        
        column.setCellValueFactory(new PropertyValueFactory<>("name"));
        orders.getColumns().add(column);
        column = new TableColumn<>("Price");        
        column.setCellValueFactory(new PropertyValueFactory<>("price"));
        orders.getColumns().add(column);
        orders.setItems(orderItems);
    }            
    
    /**
     * fill the table with the dishes
     */
    private void fillOrders(){
        try{
            MyOrder order = (MyOrder) comboBox.getSelectionModel().getSelectedItem();
            id = order.getIdOrder();

            String query = "SELECT d.* FROM toEat te "
                            + "INNER JOIN dish d ON te.dishToEat LIKE d.name "
                            + "INNER JOIN myOrder ord ON ord.idOrder=te.orderToEat "
                            + "WHERE te.orderToEat=(?) AND ord.done IS FALSE"; 
            ResultSet rs;
            try {
                System.out.println("\texecute");
                PreparedStatement prepareStatement = Main.getConnection().prepareStatement(query);
                prepareStatement.setInt(1, id);
                rs = prepareStatement.executeQuery();
                orderItems.removeAll(orderItems);
                while(rs.next()){
                    Dish item = new Dish(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getBigDecimal(4));
                    orderItems.add(item);
                    System.out.println(item);
                }
                rs.close();  
                
            } 
            catch (SQLException ex) {
                Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        catch(NullPointerException ex){
            Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        
    }
    
    /**
     * Get orders from databases and store them to comboItems
     */
    private void fillComboBox(){
        // get orders and fill the comboBox
        System.out.println("set query");
        String sql = "SELECT ord.idOrder, ord.date "
                    + "FROM myOrder ord WHERE ord.done IS FALSE "
                    + "ORDER BY date ASC";
        ResultSet rs;
        try {
            System.out.println("\texecute");
            java.sql.Statement s = Main.getStatement();
            rs = s.executeQuery(sql);
            comboItems.removeAll(comboItems);
            while(rs.next()){
                int id = rs.getInt(1);
                Timestamp timestamp = new Timestamp(rs.getTimestamp(2).getTime());
                MyOrder item = new MyOrder(id);
                item.setDate(timestamp);
                
                comboItems.add(item);
                System.out.println(item);
            }
            rs.close(); 
            comboBox.setItems(comboItems);
        } 
        catch (SQLException ex) {
            Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (NullPointerException ex){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning!!!");
            alert.setHeaderText("Probably no connection with Database!");
            
            TextArea textArea = new TextArea(ex.toString());
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(textArea, 0, 1);

            // Set expandable Exception into the dialog pane.
            alert.getDialogPane().setExpandableContent(expContent);

            alert.showAndWait();
            this.goHomeHandler(new ActionEvent());
        }
    }
    
    private void setupButtons(){
        comboBox.setOnAction(new EventHandler() {
            /**
             * Show dishes from selected order
             */
            @Override
            public void handle(Event event) {
                if ( comboBox.getSelectionModel().getSelectedItem() != null){
                    System.out.println(" -> " + comboBox.getSelectionModel().getSelectedItem());
                    fillOrders();
                }
            }
        });
        
        readyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Dish dish = (Dish) orders.getSelectionModel().getSelectedItem();                
                System.out.println("Selected: " + dish);
                orderItems.remove(dish);
                
                Collection<Contains> containsCollection = dish.getContainsCollection();
                try {                    
                    String query = "UPDATE ingredients SET available_quantity=available_quantity-(?) "
                            + "WHERE name LIKE (?)";
                    Connection conn = Main.getConnection();
                    conn.setAutoCommit(false);
                    PreparedStatement preStmt = conn.prepareStatement(query);
                    for(Contains c : containsCollection){
                        System.out.println("\t ->>> " + c);
                        preStmt.setInt(1, c.getQuantity());
                        preStmt.setString(2, c.getIngredients1().getName());
                        preStmt.addBatch();
                    }
                    int [] upCounts = preStmt.executeBatch();
                    conn.commit();
                    conn.setAutoCommit(true);
                        
                } catch (SQLException | NullPointerException ex) {
                    Logger.getLogger(ChefSceneController.class.getName()).log(Level.SEVERE, null, ex);
                }
                 
                if (orderItems.size() == 0){
                    try {
                        System.out.println("Order is ready!");
                        System.out.println("Update database");
                        
                        Connection conn = Main.getConnection();
                        String query = "UPDATE myOrder SET done=1 WHERE idOrder=(?)";
                        PreparedStatement preStmt = conn.prepareStatement(query);
                        preStmt.setInt(1, id);
                        preStmt.executeUpdate();
                        System.out.print("order "+ id + " is done!");
                        fillComboBox();
                        
                        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
                        dialog.setTitle("Notification");
                        dialog.setHeaderText("Order id: " + id);
                        dialog.setContentText("The order is ready");
                        dialog.showAndWait();
                        
                    } catch (SQLException ex) {
                        Logger.getLogger(ChefSceneController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            }
        });
        
        showInfo.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * This method it shows a message box with the 
             * ingredients and the instructions of the selected
             * dish
             */
            @Override
            public void handle(ActionEvent event) {
                Dish dish = (Dish) orders.getSelectionModel().getSelectedItem();
                MyOrder order = (MyOrder) comboBox.getSelectionModel().getSelectedItem();
                ArrayList<Contains> containsArrayList = new ArrayList();
                
                if( dish != null ){                                        
                    try {
                        String query;
                        ResultSet rs;
                        query = "SELECT COUNT(toEatID) as Counter FROM extra WHERE toEatID=(?) GROUP BY toEatID";
                        Connection connection = Main.getConnection();
                        PreparedStatement prepareStatement = connection.prepareStatement(query);
                        prepareStatement.setInt(1, order.getIdOrder());
                        rs = prepareStatement.executeQuery();
                        boolean extra = false;
                        while(rs.next()){
                            if(rs.getInt(1) > 0){
                                extra = true;
                                break;
                            }
                        }
                        
                        if(extra == false){
                            query = "SELECT ingr.name, con.quantity FROM ingredients ingr "
                                + "INNER JOIN contains con ON con.ingredients LIKE ingr.name "
                                + "WHERE con.dish LIKE (?)";
                            System.out.println("execute:\t" + query);
                            connection = Main.getConnection();
                            prepareStatement = connection.prepareStatement(query);
                            prepareStatement.setString(1, dish.getName());                            
                        }
                        else{
                            query = "SELECT e.ingredient, e.quantity FROM extra e "
                                    + "INNER JOIN toEat te ON e.toEatID=te.toEatID "
                                    + "WHERE te.orderToEat=(?) AND e.dish LIKE (?)";
                            System.out.println("execute:\t" + query);
                            connection = Main.getConnection();
                            prepareStatement = connection.prepareStatement(query);
                            prepareStatement.setInt(1, order.getIdOrder());
                            prepareStatement.setString(2, dish.getName());
                        }
                        rs = prepareStatement.executeQuery();
                        StringBuilder ingredients = new StringBuilder();
                        while(rs.next()){
                            String ingredient = rs.getString(1);
                            String quantity = Integer.toString(rs.getInt(2));
                            Contains contains = new Contains();
                            contains.setIngredients1(new Ingredients(ingredient));
                            contains.setQuantity(rs.getInt(2));
                            containsArrayList.add(contains);
                            ingredients.append("x").append(quantity).append(":\t").append(ingredient).append("\n");
                            System.out.println("\t- " + ingredient);
                        }
                        rs.close();

                        dish.setContainsCollection(containsArrayList);
                        
                        Alert info = new Alert(Alert.AlertType.INFORMATION);
                        info.setTitle(dish.getName() + " instructions");
                        info.setHeaderText(ingredients.toString());
                        info.setContentText(dish.getInstructions());
                        info.showAndWait();                        
                    } 
                    catch (SQLException ex) {
                        Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            }
        });
    }
    
    @FXML protected void goHomeHandler(ActionEvent event) {
        Stage stage;
        Parent root;

        stage = (Stage) menubar.getScene().getWindow(); 
        
        try {
            root = FXMLLoader.load(getClass().getResource("/main/mainScene.fxml"));
            Scene scene = new Scene(root, 600, 600);
            stage.setTitle("Welcome");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}