package cashier;

import com.mysql.jdbc.Connection;
import dbclasses.Client;
import dbclasses.Dish;
import dbclasses.Drinks;
import dbclasses.MyTable;
import dbclasses.Waitress;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import main.Main;
import waitress.WaitressSceneController;

/**
 * FXML Controller class
 *
 * @author dks
 */
public class CashierSceneController implements Initializable {

    @FXML MenuBar menubar;    
    
    @FXML Button checkButton;
    @FXML Button newClientButton;
    
    @FXML Label clientLabel;
    @FXML Label tableLabel;
    @FXML Label pointsLabel;
    @FXML Label ordersLabel;
    
    @FXML ComboBox comboTable;
    private final ObservableList<MyTable> comboTableItems = FXCollections.observableArrayList();
    
    @FXML ComboBox comboClient;
    private final ObservableList<Client> comboClientItems = FXCollections.observableArrayList(); 
    
    @FXML TableView orderTable;
    private final ObservableList<Dish> orderTableItems = FXCollections.observableArrayList(); 
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fillComboTable();
        fillComboClient();
        constructOrderTable();
        checkButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Client client = (Client) comboClient.getSelectionModel().getSelectedItem();
                MyTable table = (MyTable) comboTable.getSelectionModel().getSelectedItem();
                String query;
                Connection conn = Main.getConnection();
                int tmp=0;
                try {
                    orderTableItems.removeAll(orderTableItems);
                    if(client==null && table==null){
                        Alert alert = new Alert(Alert.AlertType.WARNING, "Choose client or table!", ButtonType.OK);
                        alert.showAndWait();                   
                    }
                    else if(client!=null && table==null){
                        //get dishes
                        query = "SELECT te.dishToEat, ordF.orderTable, d.price, ord.price FROM myOrder ord "
                                + "INNER JOIN orderFrom ordF ON ord.idOrder=ordF.orderID "
                                + "INNER JOIN toEat te ON te.orderToEat=ord.idOrder "
                                + "INNER JOIN client cl ON cl.card_id=ordF.orderClient "
                                + "INNER JOIN dish d ON d.name LIKE te.dishToEat "
                                + "WHERE ord.paid=false AND ordF.orderClient=(?)";  
                        PreparedStatement preStmt = conn.prepareStatement(query);
                        preStmt.setInt(1, client.getCardId());
                        ResultSet rs = preStmt.executeQuery();  
                        table = new MyTable();
                        BigDecimal total = BigDecimal.ZERO;                        
                        while(rs.next()){
                            Dish dish = new Dish(rs.getString(1));
                            dish.setPrice(rs.getBigDecimal(3));
                            table.setNumber(rs.getInt(2));                            
                            total = rs.getBigDecimal(4);
                            orderTableItems.add(dish);
                            tmp++;
                        }
                        
                        //get drinks
                        query = "SELECT d.name, d.price FROM drinks d "
                                + "INNER JOIN toDrink td ON td.drinkToDrink LIKE d.name "
                                + "INNER JOIN myOrder ord ON ord.idOrder=td.orderToDrink "
                                + "INNER JOIN orderFrom ordF ON ordF.orderID=ord.idOrder "
                                + "WHERE ord.paid=false AND ordF.orderClient=(?) ";
                        preStmt = conn.prepareStatement(query);
                        preStmt.setInt(1, client.getCardId());
                        rs = preStmt.executeQuery();  
                        while(rs.next()){
                            Drinks drink = new Drinks(rs.getString(1), rs.getBigDecimal(2));
                            orderTableItems.add(drink.toDish());
                            tmp++;
                        }
                        orderTableItems.add(new Dish("Total: ", 0, "", total));
                        if(tmp==0){
                            throw new Exception();
                        }
                        clientLabel.setText("Client: " + client.getSurname() + " " + client.getName());
                        tableLabel.setText("Table: " + table.getNumber().toString());
                        pointsLabel.setText("Points: " + Integer.toString(client.getPoints()));
                        ordersLabel.setText("Orders(month): " + Integer.toString(client.getNumOfOrders()));                        
                        
                    }                    
                    else if (table != null && client == null ){ 
                        //get dishes
                        query = "SELECT te.dishToEat, cl.surname, cl.name, cl.points, cl.num_of_orders, ordF.orderTable, d.price, ord.price FROM myOrder ord "
                                + "INNER JOIN orderFrom ordF ON ord.idOrder=ordF.orderID "
                                + "INNER JOIN toEat te ON te.orderToEat=ord.idOrder "
                                + "INNER JOIN client cl ON cl.card_id=ordF.orderClient "
                                + "INNER JOIN dish d ON d.name LIKE te.dishToEat "
                                + "WHERE ord.paid=false AND ordF.orderTable=(?)";                    
                        PreparedStatement preStmt = conn.prepareStatement(query);
                        preStmt.setInt(1, table.getNumber());
                        ResultSet rs = preStmt.executeQuery();  
                        client = new Client();                        
                        BigDecimal total = BigDecimal.ZERO;
                        while(rs.next()){
                            Dish dish = new Dish(rs.getString(1));
                            dish.setPrice(rs.getBigDecimal(7));
                            total = rs.getBigDecimal(8);
                            client.setSurname(rs.getString(2));
                            client.setName(rs.getString(3));
                            client.setPoints(rs.getInt(4));
                            client.setNumOfOrders(rs.getInt(5));
                            orderTableItems.add(dish);
                            tmp++;
                        }
                        
                        //get drinks
                        query = "SELECT d.name, d.price FROM drinks d "
                                + "INNER JOIN toDrink td ON td.drinkToDrink LIKE d.name "
                                + "INNER JOIN myOrder ord ON ord.idOrder=td.orderToDrink "
                                + "INNER JOIN orderFrom ordF ON ordF.orderID=ord.idOrder "
                                + "WHERE ord.paid=false AND ordF.orderTable=(?)";
                        preStmt = conn.prepareStatement(query);
                        preStmt.setInt(1, table.getNumber());
                        rs = preStmt.executeQuery();  
                        while(rs.next()){
                            Drinks drink = new Drinks(rs.getString(1), rs.getBigDecimal(2));
                            orderTableItems.add(drink.toDish());
                            tmp++;
                        }
                        
                        orderTableItems.add(new Dish("Total: ", 0, "", total));
                        if(tmp==0){
                            throw new Exception();
                        }
                        clientLabel.setText("Client: " + client.getSurname() + " " + client.getName());
                        tableLabel.setText("Table: " + table.getNumber().toString());
                        pointsLabel.setText("Points: " + Integer.toString(client.getPoints()));
                        ordersLabel.setText("Orders(month): " + Integer.toString(client.getNumOfOrders()));    
                    }
                    else{
                        //get dishes
                        query = "SELECT te.dishToEat, d.price, ord.price FROM myOrder ord "
                                + "INNER JOIN orderFrom ordF ON ord.idOrder=ordF.orderID "
                                + "INNER JOIN toEat te ON te.orderToEat=ord.idOrder "
                                + "INNER JOIN client cl ON cl.card_id=ordF.orderClient "
                                + "INNER JOIN dish d ON d.name LIKE te.dishToEat "
                                + "WHERE ord.paid=false AND ordF.orderTable=(?) AND ordF.orderClient=(?)";
                        PreparedStatement preStmt = conn.prepareStatement(query);
                        preStmt.setInt(1, table.getNumber());
                        preStmt.setInt(2, client.getCardId());
                        ResultSet rs = preStmt.executeQuery();                          
                        BigDecimal total = BigDecimal.ZERO;
                        while(rs.next()){
                            Dish dish = new Dish(rs.getString(1));
                            dish.setPrice(rs.getBigDecimal(2));
                            total = rs.getBigDecimal(3);
                            orderTableItems.add(dish);
                            tmp++;
                        }
                        
                        //get drinks
                        query = "SELECT d.name, d.price FROM drinks d "
                                + "INNER JOIN toDrink td ON td.drinkToDrink LIKE d.name "
                                + "INNER JOIN myOrder ord ON ord.idOrder=td.orderToDrink "
                                + "INNER JOIN orderFrom ordF ON ordF.orderID=ord.idOrder "
                                + "WHERE ord.paid=false AND ordF.orderTable=(?) AND ordF.orderClient=(?)";
                        preStmt = conn.prepareStatement(query);
                        preStmt.setInt(1, table.getNumber());
                        preStmt.setInt(2, client.getCardId());
                        rs = preStmt.executeQuery();  
                        while(rs.next()){
                            Drinks drink = new Drinks(rs.getString(1), rs.getBigDecimal(2));
                            orderTableItems.add(drink.toDish());
                            tmp++;
                        }
                        
                        orderTableItems.add(new Dish("Total: ", 0, "", total));
                        if(tmp==0){
                            throw new Exception();
                        }
                        clientLabel.setText("Client: " + client.getSurname() + " " + client.getName());
                        tableLabel.setText("Table: " + table.getNumber().toString());
                        pointsLabel.setText("Points: " + Integer.toString(client.getPoints()));
                        ordersLabel.setText("Orders(month): " + Integer.toString(client.getNumOfOrders()));    
                    }
                } 
                catch (SQLException ex) {
                    Logger.getLogger(CashierSceneController.class.getName()).log(Level.SEVERE, null, ex);
                }
                catch (Exception ex){
                    if(tmp == 0){
                        Alert alert = new Alert(Alert.AlertType.INFORMATION, 
                                    "There is no order for this client/table", ButtonType.OK);
                        alert.showAndWait();
                        clientLabel.setText("");
                        tableLabel.setText("");
                        pointsLabel.setText("");
                        ordersLabel.setText("");    
                    }
                }
                
//                aplirotes paragkelies
//                SELECT * FROM myOrder ord INNER JOIN orderFrom ordF ON ord.idOrder=ordF.orderID WHERE ord.paid=false GROUP BY ord.idOrder
            }
        });
        newClientButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Dialog inputDialog = new Dialog<>();
                inputDialog.setTitle("New Client");
                inputDialog.setHeaderText("Fill all fields");
                
                ButtonType addButtonType = new ButtonType("Add", ButtonBar.ButtonData.OK_DONE);
                inputDialog.getDialogPane().getButtonTypes().addAll(addButtonType);
                
                GridPane grid = new GridPane();
                grid.setVgap(10);
                grid.setHgap(10);
                grid.setPadding(new Insets(20, 150, 10, 10));
                
                //insert text fields
                TextField nameField = new TextField();
                nameField.setPromptText("name");
                TextField surnameField = new TextField();
                surnameField.setPromptText("surname");
                
                grid.add(new Label("Name: "), 0, 0);
                grid.add(nameField, 0, 1);
                grid.add(new Label("Surname: "), 1, 0);
                grid.add(surnameField, 1, 1);
                
                inputDialog.getDialogPane().setContent(grid);
                
                Optional<ButtonData> result = inputDialog.showAndWait();
                if(result.isPresent()){
                    try {      
                        if( nameField.getText().length()>0 && surnameField.getText().length()>0 ){
                            String query = " INSERT INTO client(name, surname) VALUES ((?), (?))";
                            System.out.println("\texecute:" + query);
                            Connection conn = Main.getConnection();
                            PreparedStatement prepareStatement = conn.prepareStatement(query);
                            prepareStatement.setString(1, nameField.getText());
                            prepareStatement.setString(2, surnameField.getText());
                            prepareStatement.executeUpdate();
                            System.out.println("insert OK!");
                            fillComboClient();
                        }
                        else{
                            throw new Exception();
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(CashierSceneController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Alert alert = new Alert(Alert.AlertType.WARNING, "Fill name and username", ButtonType.OK);
                        alert.showAndWait();
                    }
                }
                
            }
        });
    }  
    
    private void fillComboTable(){
        System.out.println("set query");
        String sql = "SELECT number, capacity, description, waitress FROM mytable";
        ResultSet rs;
        try {
            System.out.println("\texecute -> " + sql);
            java.sql.Statement s = Main.getStatement();
            rs = s.executeQuery(sql);
            comboTableItems.removeAll(comboTableItems);
            comboTableItems.add(null);
            while(rs.next()){
                Integer tmpNumber = new Integer(rs.getInt("number"));
                Integer tmpCapacity = new Integer(rs.getString("capacity"));
                String tmpDescription = rs.getString("description");                
                Waitress tmpWaitress = new Waitress(rs.getInt("waitress"));              
                MyTable item = new MyTable(tmpNumber, tmpCapacity, tmpDescription, tmpWaitress);
                comboTableItems.add(item);
                System.out.println(item);
            }
            rs.close();  
            comboTable.setItems(comboTableItems);
        } 
        catch (SQLException ex) {
            Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void fillComboClient(){
        System.out.println("set query");
        String sql = "SELECT card_id, surname, name, points, num_of_orders FROM client";
        ResultSet rs;
        try {
            System.out.println("\texecute -> " + sql);
            java.sql.Statement s = Main.getStatement();
            rs = s.executeQuery(sql);
            comboClientItems.removeAll(comboClientItems);
            comboClientItems.add(null);
            while(rs.next()){
                Integer cardId = new Integer(rs.getInt(1));
                String surname = rs.getString(2);
                String name = rs.getString(3);                
                Client item = new Client(cardId, surname, name);
                item.setPoints(rs.getInt(4));
                item.setNumOfOrders(rs.getInt(5));
                comboClientItems.add(item);
                System.out.println(item);
            }
            rs.close();  
            comboClient.setItems(comboClientItems);
        } 
        catch (SQLException ex) {
            Logger.getLogger(WaitressSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void constructOrderTable(){
        TableColumn column = new TableColumn<>("Name");        
        column.setCellValueFactory(new PropertyValueFactory<>("name"));
        orderTable.getColumns().add(column);
        column = new TableColumn<>("Price");        
        column.setCellValueFactory(new PropertyValueFactory<>("price"));
        orderTable.getColumns().add(column);
        orderTable.setItems(orderTableItems);
    }
    
    @FXML protected void goHomeHandler(ActionEvent event) {
        Stage stage;
        Parent root;

        stage = (Stage) menubar.getScene().getWindow(); 
        
        try {
            root = FXMLLoader.load(getClass().getResource("/main/mainScene.fxml"));
            Scene scene = new Scene(root, 600, 600);
            stage.setTitle("Welcome");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
