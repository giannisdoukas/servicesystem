/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visualizer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author dks
 */
public class VTable extends TableView{

    public VTable(Object obj) {        
        
        System.out.println("colums names: ");
        for(Method m : obj.getClass().getMethods()){
            try{
                if(m.getName().startsWith("get_") && m.getParameterTypes().length == 0){
                    System.out.println(m.invoke(obj));
                    String colName = m.getName().replaceFirst("get_", "");
                    if(colName.compareToIgnoreCase("Class") != 0){
                        TableColumn column = new TableColumn(colName);
                        column.setCellValueFactory(new PropertyValueFactory("_" + colName));
                        this.getColumns().add(column);
                        System.out.println("\t->" + colName);
                    }
                }
            }
            catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException ex){
                System.err.println(ex.toString());
            }
        }
    }
    
}
